/**
  ******************************************************************************
  * @file     As5045.h
  * @author		Alejandro Gast�n Alvarez
  * @version	1.0
  * @date			15/03/2016
  * @brief		Driver para los sensores AS5045
  ******************************************************************************
  * @attention
  *
  ******************************************************************************
	*/

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef _AS5045_H
#define _AS5045_H

/* Includes ------------------------------------------------------------------*/
#include <lpc2148_hal_def.h>
#include <stdbool.h>
#include <stddef.h>
#include <ring_buffer.h>

/* Typedef -----------------------------------------------------------------------------------------*/
static RingBuffer* encoder_buffer;

typedef enum
{
	AS5045_Pulling,
	AS5045_Interrupt,
} AS5045_Mode;


typedef struct
{
	uint8_t EvenPar						: 1;
	bool mag_dec							: 1;
	bool mag_inc							: 1;
	bool linearity_alarm			: 1;
	bool cordic_overflow			: 1;
	bool offset_comp_finished	: 1;
	uint16_t 									: 0;
	uint16_t ang_pos 					: 12;
} AS5045_Data_Type;
	
typedef enum
{
	AS5045_OK = 0,
	AS5045_PARITY_ERROR = 0x01,
	AS5045_COMP_ALGO_NOT_FINISHED = 0x02,
	AS5045_CORDIC_OVERFLOW = 0x04,
	AS5045_MAGNET_BAD_ALIGMENT = 0x08,
	AS5045_MAGNET_MOVING = 0x10,
	AS5045_MAGNET_INSTABLE = 0x20,
}AS5045_Status;

void As5045_Init ( RingBuffer* encoder_buffer, const size_t buff_size );
void AS5045AskData ( void );
AS5045_Status ValData (AS5045_Data_Type *data);

#endif // _AS5045_H_
