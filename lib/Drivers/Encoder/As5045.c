/**
  ******************************************************************************
  * @file    	main.c
  * @author  ale
  * @version
  * @date
  * @brief
  ******************************************************************************
  * @attention
  * (the sensor works in Daisy Chain Mode)
  ******************************************************************************
	*/

#include <As5045.h>
#include <lpc2148_hal.h>


/* Functions -----------------------------------------------------------------------------------------*/

void As5045_Init( RingBuffer* clt_buffer, const size_t buff_size )
{
	encoder_buffer = clt_buffer;
	
	// Seteo los pines P0.17-18-19 (SCK,MOSI,MISO) como SSI-SSP
	P0ModeSelec (PIN_17 | PIN_18 | PIN_19, P0_MODE_SSP, 0);
	
	//El SSEL lo hago manual porque sino me corta la comunicaci�n
	P0ModeSelec (PIN_20, P0_MODE_GPIO, 1); 
	
	// Desactivo la lectura del puerto SSI-SSP
	Pin_Set (PORT_0, PIN_20);	

	// Create Ring Buffer
	ringBuf_init ( encoder_buffer, buff_size * sizeof(AS5045_Data_Type) , false );

	// Configure peripheric
	initSSP(SSP_Master, SSP_SPI, CLK_POL_HIGH, SSP_PH_FRST, SSP_Tx10, 50000, SSP_IRQ_Rx_Half_Full, 5);
//	SSP_LoopBack_Enable (true);
	
	// Enable SSP
	SSP_Enable (true);
}

void AS5045AskData ( void )
{
	// Seteo el valor a enviar
	int dummy = 0xA5A5;
	
	// Activo la lectura de los senosres
	Pin_Clear (PORT_0, PIN_20);
	
	// Env�o 36 bits (escribo 4 de 9 bits)
	for (uint8_t i=0; i<4; i++)
		while(writeSSP (dummy) != SSP_OK);
}

/**
 * @brief	Valida la trama.
 * @details	Analiza la trama recibida y los flags de estado para validar el angulo recibido
 * @param data trama recibida de un sensor
 * @return Status  	
 */
AS5045_Status ValData (AS5045_Data_Type *data)
{
	// Como AS5045_Data_Type es de 32bits puedo utilizar esta variable y despu�s la casteo
	uint32_t* sensor = (uint32_t*) data;
	AS5045_Status status = AS5045_OK;
	
	// Check Parity
	uint8_t parity = 0;
	for (uint32_t i=1; i < sizeof(sensor)*8; i++) // Comienzo desde 1 para no tener en cuenta el bit de Even Parity
		parity ^= ( (*sensor >> i) & 0x01 );
	
	if (data->EvenPar != parity)
		status += AS5045_PARITY_ERROR;

	// Check Offset Compensation Finished (OCF)
	if (!data->offset_comp_finished)
		status += AS5045_COMP_ALGO_NOT_FINISHED;

	// Check Cordic Overflow
	if (data->cordic_overflow)
		status += AS5045_CORDIC_OVERFLOW;
	
	// Linearity Alarm check
	if (data->linearity_alarm)
		status += AS5045_MAGNET_BAD_ALIGMENT;

	// Check Magnet Status
	if (data->mag_inc && data->mag_dec)
		status += AS5045_MAGNET_INSTABLE;
	else if (data->mag_inc || data->mag_dec)
		status += AS5045_MAGNET_MOVING;
	
	return status;
}

__irq void SSP_IRQ (void)
{
	// TODO: verificaci�n de errores y otras interrupciones
	
	// Desactivo la lectura del puerto SSI-SSP
	Pin_Set (PORT_0, PIN_20);
	
	// Read sensor data (SPP FIFO Read buffer)
	// Data is packed 4 words of 16bits with 10 bits of valid data
	uint16_t data[4] = {0,0,0,0};
	uint8_t i=0;
	for(; i<4; i++)
		while (readSSP(&data[i]) != SSP_OK);
	
	// Si no se lleg� a leer 4 datos ==> hubo algun error
	if (i == 4)
	{
		union
		{
			AS5045_Data_Type data_as5045;
			uint32_t data_int;
		} sensor[2] = {0,0}; // Como AS5045_Data_Type es de 32bits puedo utilizar esta variable y despu�s la casteo
	
		// Arrange data (data is package in 10 bits)
		sensor[0].data_int = ((data[0] & 0x1FF) << 19) + ((data[1] & 0x380) << 9);	// dejo en los 16 bits m�s altos el valor de la posici�n angular
		sensor[0].data_int += (data[1] & 0x07E) >> 1; // dejo en los 16 bits m�s bajos los datos de informaci�n del encoder
		
		sensor[1].data_int = ((data[2] & 0x3FF) << 18) + ((data[3] & 0x300) << 8);  // dejo en los 16 bits m�s altos el valor de la posici�n angular
		sensor[1].data_int += (data[3] & 0xFC) >> 2; // dejo en los 16 bits m�s bajos los datos de informaci�n del encoder
		
		// Validate data and if it's ok write data on the buffer
//		if ( ValData( (AS5045_Data_Type*) &sensor[0] ) == AS5045_OK && ValData( (AS5045_Data_Type*) &sensor[1] ) == AS5045_OK )
//		if ( ValData( &sensor[0].data_as5045 ) == AS5045_OK && ValData( &sensor[1].data_as5045 ) == AS5045_OK )
			ringBuf_write ( encoder_buffer, (uint8_t*) sensor, 2 * sizeof(AS5045_Data_Type) );
	}
	
	// Clean Interruption
	VICVectAddr = 0;
}
