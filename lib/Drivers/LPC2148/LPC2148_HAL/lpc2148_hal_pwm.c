/**
  ******************************************************************************
  * @file    lpc2148_hal_pwm.h
  * @author  Alejandro Alvarez
  * @version 0.1
  * @date    04/10/2015
  * @brief   Funciones para el control de periféricos del PWM
  ******************************************************************************
  * @attention
  *
  ******************************************************************************
	*/
	
#include <lpc2148_hal_pwm.h>


//TODO Sacar de aca
#define pfreq 30000000


int initPWM(float pwm_tick, float pwm_freq)
{
	// Disable and reset PWM
	PWMTCR &= ~COUNTER_ENABLE;
	PWMTCR &= ~PWM_ENABLE;
	PWMTCR |= COUNTER_RESET;	
		
	// Set Prescaler register (pwm_clock in micro-seg)
	PWMPR = (uint32_t) (pwm_tick * (pfreq/1000000))-1;
	
	// Set Match 0 Register
	PWMMR0 = (uint32_t) 1 / (pwm_tick * (pwm_freq/1000));
	PWMMCR = PWM_RESET_COUNTER;
	
	return 0;
}

//pwm_freq / (pfreq/100000 / pwm_tick )
void deInitPWM(void)
{
	// Disable and reset PWM
	PWMTCR &= ~COUNTER_ENABLE;
	PWMTCR &= ~PWM_ENABLE;
	PWMTCR |= COUNTER_RESET;
	
	// Set Match 0 Registers to 0
	PWMMR0 = 0;
	PWMMR1 = 0;
	PWMMR2 = 0;
	PWMMR3 = 0;
	PWMMR4 = 0;
	PWMMR5 = 0;
	PWMMR6 = 0;
	
	// Disable all PWMs
	PWMPCR = 0;
}

void configPWMxChannel(PWMx pwm_channel, PWM_Edge_Type PWM_Edge_Type, PWM_Action action, uint32_t duty, uint32_t offset, uint8_t irq_priotity) // duty en porcentaje y offset en microsegundos
{
//	float pfreq = 30000000;
//	float pwm_clock = PWMPR / (pfreq/1000000); //En micro-segundos
	
	// Set edge type
	if (PWM_Edge_Type == PWM_DOUBLE_EDGE)
		PWMPCR |= 1 << pwm_channel;
	else if (PWM_Edge_Type == PWM_SINGLE_EDGE)
		PWMPCR &= ~(1 << pwm_channel);
	
	// Set Match Register
	volatile unsigned long * PWMMRx = (volatile unsigned long *) ((uint32_t) &PWMMR0 + pwm_channel * sizeof(uint32_t));
	
	if (PWM_Edge_Type == PWM_SINGLE_EDGE)
	{		
		*PWMMRx = (float) PWMMR0 * duty / 100;
	}
	else
	{		
		*PWMMRx = (float) (offset + PWMMR0) * duty / 100;
		*(PWMMRx-4) = (float) offset * duty / 100;
	}
	
	//Set Action
	PWMMCR |= action << (pwm_channel * 3);
	
	if (action && PWM_ACTION_INTERRUPT)
	{
		VIC_EnableIRQ(PWM0_IRQn, 2, (uint32_t*)irqPWM, false);
		VIC_DisableIRQ(PWM0_IRQn);
	}
}


void startPWM(void)
{
	// Start PWM Counter
	PWMTCR = COUNTER_ENABLE | PWM_ENABLE;	
}

void stopPWM(void)
{
	// Stop PWM Counter
	PWMTCR = COUNTER_RESET;	
}


void enablePWMxChannel(PWMx pwm_channel)
{
	PWMPCR |= 1 << (pwm_channel + 8);
}

void disablePWMxChannel(PWMx pwm_channel)
{
	PWMPCR &= ~(1 << (pwm_channel + 8));
}

void updatePWMxChannel(PWMx pwm_channel, uint32_t new_duty, uint32_t new_offset)
{
	// Set Match Register
	volatile unsigned long * PWMMRx = (volatile unsigned long *) ((uint32_t) &PWMMR0 + pwm_channel * sizeof(uint32_t));
	
	if (! IS_DOUBLE_EDGE(pwm_channel) )
	{		
		*PWMMRx = (float) PWMMR0 * new_duty / 100;
	}
	else
	{		
		*PWMMRx = (float) (new_offset + PWMMR0) * new_duty / 100;
		*(PWMMRx-4) = (float) new_offset * new_duty / 100;
	}
	
	//PWMLER = 1 << pwm_channel;
	setReg32(PWMLER, 0x7F);        			 //Actualiza todos los registros de match de cada PWM al mismo tiempo.
}

uint8_t getPWMInterruptStatus(void)
{
	uint16_t temp = PWMIR;
	
	uint8_t interrupts = (temp & 0x0F) | ((temp & 0x700)>>4);
	
	return interrupts;
}

__weak void irqPWM (void)
{
		// EL USUARIO LA TIENE QUE REESCRIBIR
}

