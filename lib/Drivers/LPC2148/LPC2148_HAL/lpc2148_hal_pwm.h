/**
  ******************************************************************************
  * @file    lpc2148_hal_pwm.h
  * @author  Alejandro Alvarez
  * @version 0.1
  * @date    04/10/2015
  * @brief   Funciones para el control de periféricos del PWM
  ******************************************************************************
  * @attention
  *
  ******************************************************************************
	*/

#ifndef _LPC2148_HAL_PWM_H
#define _LPC2148_HAL_PWM_H

#include <lpc2148_hal.h>

#define COUNTER_ENABLE	((uint32_t)(1<<0))
#define COUNTER_RESET		((uint32_t)(1<<1))
#define PWM_ENABLE			((uint32_t)(1<<3))

#define PWM_ACTION_INTERRUPT			((uint32_t)(1<<0))
#define PWM_ACTION_RESET_COUNTER	((uint32_t)(1<<1))
#define PWM_ACTION_STOP_COUNTER		((uint32_t)(1<<2))

#define PWM_SET_SINGLE_EDGE		((uint32_t)(0<<0))
#define PWM_SET_DOUBLE_EDGE		((uint32_t)(1<<1))

#define IS_DOUBLE_EDGE(pwm_channel)				(PWMPCR & (1 << pwm_channel))

/** PWM Channels
 */
typedef enum
{
	PWM1 = 1,
	PWM2,
	PWM3,
	PWM4,
	PWM5,
	PWM6,
}PWMx;

/** PWM Edge Types
 * 
 */
typedef enum
{
	PWM_SINGLE_EDGE = 0,
	PWM_DOUBLE_EDGE = 1
}PWM_Edge_Type;

typedef enum
{
	PWN_NONE = 0,
	PWM_INTERRUPT = PWM_ACTION_INTERRUPT,
	PWM_RESET_COUNTER = PWM_ACTION_RESET_COUNTER,
	PWM_STOP_COUNTER = PWM_ACTION_STOP_COUNTER,
	PWM_INT_RESET = PWM_ACTION_INTERRUPT | PWM_ACTION_RESET_COUNTER,
	PWM_INT_STOP = PWM_ACTION_INTERRUPT | PWM_ACTION_STOP_COUNTER,
	PWM_RESET_STOP = PWM_ACTION_RESET_COUNTER |PWM_ACTION_STOP_COUNTER,
}PWM_Action;

typedef enum
{
	PWM_SUCCESS,
	PWM_ERROR,
}PWM_Status;

/**
 * @brief Función de inicialización del periférico PWM
 * @details Setea los registros generales del PWM pero no activa ningún canal
 * 
 * @param pwm_tick Tick del contador del PWM [mseg]
 * @param pwm_freq Frecuencia del PWM [KHz]
 * 
 * @return Error Status
 */
int initPWM(float pwm_clk, float pwm_freq);
/**
 * @brief De-inicializa el PWM
 * @details Coloca todos los registros en el estado de reset
 */
void deInitPWM(void);
/**
 * @brief Comienza el PWM
 * @details Se habilita el contador del PWM, pero no implica que las salidas se encuentren habilitadas
 */
void startPWM(void);
/**
 * @brief Se para el PWM
 * @details Se deshabilita el contador del PWM y se reseetea.
 */
void stopPWM(void);
/**
 * @brief Configura un canal del PWM
 * @details Configura el canal del PWM especificado, pero no lo activa eso se debe hacer mediante la función enablePWMx
 * 
 * @param pwm_channel Selección del Canal del PWM a configurar
 * @param PWM_Edge_Type Tipo de flanco del PWM (Doble o Simple) (mirar la hoja de datos)
 * @param action Acción a tomar cuando se cumpla el tiempo establecido
 * @param duty Duty cycle del canal del PWM (con esto se setea el tiempo de subida - para Single-Edge el de bajada es siempre Match0)
 * @param offset Valor de Offset del pulso del PWM (solo sirve en Doble-Edge)
 * @param irq_priotity Prioridad de la Interrupción del PWM (solo sirve cuando se setea acción en modo de interrupción)
 */
void configPWMxChannel(PWMx pwm_channel, PWM_Edge_Type PWM_Edge_Type, PWM_Action action, uint32_t duty, uint32_t offset, uint8_t irq_priotity);
/**
 * @brief Habilita la salida del canal especificado
 * @details Habilita la salida del canal especificado
 * 
 * @param pwm_channel Canal de PWM a habilitar
 */
void enablePWMxChannel(PWMx pwm_channel);
/**
 * @brief Deshabilita la salida del canal especificado
 * @details [long description]
 * @details Deshabilita la salida del canal especificado
 * 
 * @param pwm_channel Canal de PWM a deshabilitar
 */
void disablePWMxChannel(PWMx pwm_channel);
/**
 * @brief Actualiza el Duty y el Offset del canal especificado
 * @details Actualiza los Match registers del canal especificado para actualizarlos según el Duty y el Offset especificado
 * 
 * @param pwm_channel Canal de PWM a actualizar
 * @param new_duty Nuevo Duty cycle de la salida (se indica el duty de la salida en alto)
 * @param new_offset Nuevo Offset pwm (solo se utiliza si es Double_Edge)
 */
void updatePWMxChannel(PWMx pwm_channel, uint32_t new_duty, uint32_t new_offset);
/**
 * @brief Estado de Interrupción del PWM
 * @details Indica si algún match register generó interrupción. Es un vector de 8 bits donde cada bit indica el Match correspondiente
 * 			Bits ==>   7    6     5     4     3     2     1     0
 * 	Match Register ==> 0 - MR6 - MR5 - MR4 - MR3 - MR2 - MR1 - MR0
 * @return Estado de la interrupción
 */
uint8_t getPWMInterruptStatus(void);

/**
 * @brief Función de interrupción del PWM
 * @details Esta función debe ser sobreescrita por el usuario
 */
__weak void irqPWM (void);


#endif									/* < End _LPC2148_HAL_PWM_H*/
