/**
  ******************************************************************************
  * @file    lpc2148_hal_ssp.h
  * @author  Alejandro Gast�n Alvaerez
  * @version 1.0
  * @date    09/01/2016
  * @brief   SSP Hardware Abstraction Layer (HAL)
  ******************************************************************************
  * @attention
  *
  ******************************************************************************
	*/

#ifndef _LPC2148_HAL_SSP_H
#define _LPC2148_HAL_SSP_H


#include <lpc2148_hal.h>

// SSP Status (SSPICR)
#define	SSP_TxFIFO_Empty 			0x01
#define	SSP_TxFIFO_Not_Full   0x02
#define SSP_RxFIFO_Not_Empty  0x04
#define	SSP_RxFIFO_Full  			0x08
#define	SSP_Busy         			0x10

#define IS_SSP_TxFIFO_Empty() 		((bool) (SSPSR & SSP_TxFIFO_Empty))
#define IS_SSP_TxFIFO_Not_Full()  ((bool) (SSPSR & SSP_TxFIFO_Not_Full))
#define IS_SSP_RxFIFO_Not_Empty() ((bool) (SSPSR & SSP_RxFIFO_Not_Empty))
#define IS_SSP_RxFIFO_Full()  		((bool) (SSPSR & SSP_RxFIFO_Full))
#define IS_SSP_Busy() 			 			((bool) (SSPSR & SSP_Busy))

// Interrupt Reasons (SSPICR)
#define SSP_IRQ_DISABLE			 0x00
#define	SSP_IRQ_Rx_Full			 0x01
#define	SSP_IRQ_Rx_Time_Out  	 0x02
#define	SSP_IRQ_Rx_Half_Full   0x04
#define	SSP_IRQ_Tx_Half_Empty  0x08

#define IS_SSP_IRQ_Rx_Half_Full() 	((bool) (SSPSR & SSP_IRQ_Rx_Half_Full))
#define IS_SSP_IRQ_Tx_Half_Empty()  ((bool) (SSPSR & SSP_IRQ_Tx_Half_Empty))
__inline bool IS_SSP_IRQ_Rx_Full(void) 	{bool state = SSPSR & SSP_IRQ_Rx_Full;		SSPICR |= SSP_IRQ_Rx_Full;		return state;}
__inline bool IS_SSP_IRQ_Rx_Time_Out (void)  	{bool state = SSPSR & SSP_IRQ_Rx_Time_Out;		SSPICR |= SSP_IRQ_Rx_Time_Out;		return state;}



typedef enum{
	CLK_POL_HIGH = 1,
	CLK_POL_LOW  = 0,
}SSP_CLK_Pol;

typedef enum{
	SSP_PH_FRST = 0,
	SSP_PH_SCND = 1,
}SSP_PHASE;

typedef enum{
	SSP_Slave  = 1,
	SSP_Master = 0,
}SSP_Modes;

typedef enum{
	SSP_SPI = 0,
	SSP_SSI = 1,
	SSP_Microwire = 2,
}SSP_FORMAT;

typedef enum{
	SSP_Tx04 = 0x03,
	SSP_Tx05 = 0x04,
	SSP_Tx06 = 0x05,
	SSP_Tx07 = 0x06,
	SSP_Tx08 = 0x07,
	SSP_Tx09 = 0x08,
	SSP_Tx10 = 0x09,
	SSP_Tx11 = 0x0A,
	SSP_Tx12 = 0x0B,
	SSP_Tx13 = 0x0C,
	SSP_Tx14 = 0x0D,
	SSP_Tx15 = 0x0E,
	SSP_Tx16 = 0x0F,
}SSP_TxBits;

typedef enum{
	SSP_Error = -1,
	SSP_OK = 0,
	SSP_is_Busy = 1,
}SSP_Status;




/**
 * @brief Configures the SSP.
 * @details Configures the Synchronous Serial Port (SSP) 0.
 *
 * @param mode					SSP mode selection
 * @param format				SSP format selection
 * @param clkpol				SSP clock polarity
 * @param phase					SSP phase polarity
 * @param num_bits			number of bits to transmit
 * @param ssp_freq			SSP frequency selection
 * @param IRQ_Masks			SSP IRQ mask
 * @param IRQ_priority	SSP IRQ priority
 * @return returns SSP status.
 */
SSP_Status initSSP(SSP_Modes mode, SSP_FORMAT format, SSP_CLK_Pol clkpol, SSP_PHASE phase, SSP_TxBits num_bits, int ssp_freq, int IRQ_Masks, int IRQ_priority);
/**
 * @brief Restores SSP configuration.
 * @details Sets SSP configuration to default (reset) state.
 * @return returns SSP status.
 */
SSP_Status deinitSSP(void);
/**
 * @brief Gets SSP status.
 * @details Consults and returns SSP status.
 * @return returns SSP frequency.
 */
int stateSSP (void);
/**
 * @brief Writes the SSP buffer.
 * @details Writes the SSP buffer. When the SSP is in Master mode, this automatically starts an SSP communication.
 *
 * @param data Data to be transmitted.
 */
SSP_Status writeSSP (int data);
/**
 * @brief Reads the SSP buffer.
 * @details Reads the SSP buffer.
 *
 * @param data Pointer to a variable where the data should be stored.
 */
SSP_Status readSSP (uint16_t *data);
/**
 * @brief Enable or Disable SSP output.
 * @details Enable or Disable SSP output.
 *
 * @param state True if SSP should be enable, otherwise false
 */
SSP_Status SSP_Enable (bool state);
/**
 * @brief Enable or disable LoopBack Mode.
 * @details Enable or disable LoopBack Mode.
 *
 * @param state True if the LoopBack Mode should be enable, otherwise false
 */
SSP_Status SSP_LoopBack_Enable (bool state);
/**
 * @brief Calculates best prescaler values.
 * @details Calculates SCR and CPSDVSR bytes in orther to obtain the best match with the desired output and the peripherial clock settings
 *
 * @param[in] 	 freq desired SSP frequency
 * @param[inout] scr  pointer to an int where the SCR value will be store
 * @param[inout] cpsdvsr pointer to an int where the CPSDVSR value will be store
 */
static void freqcalc (uint32_t freq, uint8_t* scr, uint8_t* cpsdvsr);
/**
 * @brief Interrupt Function of the SSP
 * @details This function must be filled by the user
 */
__weak __irq void SSP_IRQ (void);

#endif	// End _LPC2148_HAL_SSP_H
