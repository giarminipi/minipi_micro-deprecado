/**
  ******************************************************************************
  * @file    lpc2148_hal_tim.c
  * @author  JMGG & JSTC & AB
  * @version 4.0
  * @date    08/03/2016
  * @brief   Timers initialization and configuration
  ******************************************************************************
  * @attention
  *
  ******************************************************************************
	*/
	
#include <lpc2148_hal.h>

void init_pll (void) {
	
	//Inicializacion freq. CPU y Perifericos	cclk=60mhz y pclk=30mhz
	setReg32(PLL0CFG, 0x24); 					//Setea multiplicador M y divisor para dar 60MHz
	setReg32(PLL0CON, 0x1);						//Habilita PLL
	setReg32(PLL0FEED, 0xAA);					//Actualiza los registros del PLL con la secuencia FEED
	setReg32(PLL0FEED, 0x55); 
	while(!(PLL0STAT & 0x00000400));	//Testea Lock bit  
	setReg32(PLL0CON, 0x03);					//Conecta el PLL
	setReg32(PLL0FEED, 0xAA); 				//Actualiza los registros del PLL con la secuencia FEED
	setReg32(PLL0FEED, 0x55);			
	setReg32(VPBDIV, 0x2); 	 					//Setea el bus de perifericos a 30MHz
}

void init_Timer (uint32_t prior, uint32_t timer, uint32_t mode, uint32_t pin, uint32_t temp){

	switch (timer){
		
		case TIMER_0:
									switch(mode){
									
										case MODE_CAP_RISE:
																				P0ModeSelec (pin , P0_MODE_CAPTURE, 0);
																				switch(pin) {
																					case PIN_2:
																					case PIN_22:
																					case PIN_30:
																												setReg32(T0CCR,T0CCR & 0xFFFFFFF8);
																												setReg32(T0CCR,T0CCR | 0x00000005);
																												break;
																					case PIN_4:
																												setReg32(T0CCR,T0CCR & 0xFFFFFFC7);
																												setReg32(T0CCR,T0CCR | 0x00000028);
																												break;
																					case PIN_6:
																					case PIN_16:
																					case PIN_28:
																												setReg32(T0CCR,T0CCR & 0xFFFFFE3F);
																												setReg32(T0CCR,T0CCR | 0x00000140);
																												break;
																					case PIN_29:
																												setReg32(T0CCR,T0CCR & 0xFFFFF1FF);
																												setReg32(T0CCR,T0CCR | 0x00000A00);
																												break;
																				}
																				setReg32(T0CTCR , 0x00000000);
																				setReg32(T0PR , 3*temp-1);
																				break;
										
										case MODE_CAP_FALL:
																				P0ModeSelec (pin , P0_MODE_CAPTURE, 0);
																				switch(pin) {
																					case PIN_2:
																					case PIN_22:
																					case PIN_30:
																												setReg32(T0CCR,T0CCR & 0xFFFFFFF8);
																												setReg32(T0CCR,T0CCR | 0x00000006);
																												break;
																					case PIN_4:
																												setReg32(T0CCR,T0CCR & 0xFFFFFFC7);
																												setReg32(T0CCR,T0CCR | 0x00000030);
																												break;
																					case PIN_6:
																					case PIN_16:
																					case PIN_28:
																												setReg32(T0CCR,T0CCR & 0xFFFFFE3F);
																												setReg32(T0CCR,T0CCR | 0x00000180);
																												break;
																					case PIN_29:
																												setReg32(T0CCR,T0CCR & 0xFFFFF1FF);
																												setReg32(T0CCR,T0CCR | 0x00000C00);
																												break;
																				}
																				setReg32(T0CTCR , 0x00000000);
																				setReg32(T0PR , 3*temp-1);
																				break;
										
										case MODE_CAP_BOTH:
																				P0ModeSelec (pin , P0_MODE_CAPTURE, 0);
																				switch(pin) {
																					case PIN_2:
																					case PIN_22:
																					case PIN_30:
																												setReg32(T0CCR,T0CCR & 0xFFFFFFF8);
																												setReg32(T0CCR,T0CCR | 0x00000007);
																												break;
																					case PIN_4:
																												setReg32(T0CCR,T0CCR & 0xFFFFFFC7);
																												setReg32(T0CCR,T0CCR | 0x00000038);
																												break;
																					case PIN_6:
																					case PIN_16:
																					case PIN_28:
																												setReg32(T0CCR,T0CCR & 0xFFFFFE3F);
																												setReg32(T0CCR,T0CCR | 0x000001C0);
																												break;
																					case PIN_29:
																												setReg32(T0CCR,T0CCR & 0xFFFFF1FF);
																												setReg32(T0CCR,T0CCR | 0x00000E00);
																												break;
																				}
																				setReg32(T0CTCR , 0x00000000);
																				setReg32(T0PR , 3*temp-1);
																				break;
										
										case MODE_TEMP:
																				break;
										
										default:						
																				break;
									}
									VIC_EnableIRQ(TIMER0_IRQn, prior, (uint32_t*)INTERRUPT_TIMER0, false);
									setReg32(T0TCR,0x00000002);		// Resetea contador y prescaler
									setReg32(T0TCR,0x00000001);		// Habilita contador y prescaler
									break;
	
		case TIMER_1:
									switch(mode){
									
										case MODE_CAP_RISE:
																				P0ModeSelec (pin , P0_MODE_CAPTURE, 0);
																				switch(pin) {
																					case PIN_10:
																												setReg32(T1CCR,T1CCR & 0xFFFFFFF8);
																												setReg32(T1CCR,T1CCR | 0x00000005);
																												break;
																					case PIN_11:
																					case PIN_21:
																												setReg32(T1CCR,T1CCR & 0xFFFFFFC7);
																												setReg32(T1CCR,T1CCR | 0x00000028);
																												break;
																					case PIN_17:
																												setReg32(T1CCR,T1CCR & 0xFFFFFE3F);
																												setReg32(T1CCR,T1CCR | 0x00000140);
																												break;
																					case PIN_18:
																												setReg32(T1CCR,T1CCR & 0xFFFFF1FF);
																												setReg32(T1CCR,T1CCR | 0x00000A00);
																												break;
																				}
																				setReg32(T1CTCR , 0x00000000);
																				setReg32(T1PR , 3*temp-1);
																				break;
										
										case MODE_CAP_FALL:
																				P0ModeSelec (pin , P0_MODE_CAPTURE, 0);
																				switch(pin) {
																					case PIN_10:
																												setReg32(T1CCR,T1CCR & 0xFFFFFFF8);
																												setReg32(T1CCR,T1CCR | 0x00000006);
																												break;
																					case PIN_11:
																					case PIN_21:
																												setReg32(T1CCR,T1CCR & 0xFFFFFFC7);
																												setReg32(T1CCR,T1CCR | 0x00000030);
																												break;
																					case PIN_17:
																												setReg32(T1CCR,T1CCR & 0xFFFFFE3F);
																												setReg32(T1CCR,T1CCR | 0x00000180);
																												break;
																					case PIN_18:
																												setReg32(T1CCR,T1CCR & 0xFFFFF1FF);
																												setReg32(T1CCR,T1CCR | 0x00000C00);
																												break;
																				}
																				setReg32(T1CTCR , 0x00000000);
																				setReg32(T1PR , 3*temp-1);
																				break;
										
										case MODE_CAP_BOTH:
																				P0ModeSelec (pin , P0_MODE_CAPTURE, 0);
																				switch(pin) {
																					case PIN_10:
																												setReg32(T1CCR,T1CCR & 0xFFFFFFF8);
																												setReg32(T1CCR,T1CCR | 0x00000007);
																												break;
																					case PIN_11:
																					case PIN_21:
																												setReg32(T1CCR,T1CCR & 0xFFFFFFC7);
																												setReg32(T1CCR,T1CCR | 0x00000038);
																												break;
																					case PIN_17:
																												setReg32(T1CCR,T1CCR & 0xFFFFFE3F);
																												setReg32(T1CCR,T1CCR | 0x000001C0);
																												break;
																					case PIN_18:
																												setReg32(T1CCR,T1CCR & 0xFFFFF1FF);
																												setReg32(T1CCR,T1CCR | 0x00000E00);
																												break;
																				}
																				setReg32(T1CTCR , 0x00000000);
																				setReg32(T1PR , 3*temp-1);
																				break;
										
										case MODE_TEMP:
																				break;
										
										default:						
																				break;
									}
									VIC_EnableIRQ(TIMER1_IRQn, prior, (uint32_t*)INTERRUPT_TIMER1, false);
									setReg32(T1TCR,0x00000002);		// Resetea contador y prescaler
									setReg32(T1TCR,0x00000001);		// Habilita contador y prescaler									
									break;
	}
}


void init_T0(void){	
	
	// Configuracion para tomar los sensores de ultrasonido
		  P0ModeSelec (PIN_22 , P0_MODE_CAPTURE, 0);
			P0ModeSelec (PIN_6 , P0_MODE_CAPTURE, 0);
	
//	CAP0.0 (P0.2 - P0.22 - P0.30)
//	CAP0.1 (P0.4)
//	CAP0.2 (P0.6 - P0.16 - P0.28)
//	CAP0.3 (P0.29)
	                
	

		setReg32(T0CCR,0x000001C7);
				// Captura flanco ascendente y descendente en los canales CAP0.2(P0.6) y CAP0.0(P0.22)

	
				// Configuracion del canal de captura, incluyendo el modo de captura.

    setReg32(T0CTCR , 0x00000000);
				// [7:2]--> reservado
				// [1]--> Counter Reset
				// [0]--> Counter Enable

    setReg32(T0PR , PRESCALE_T0-1);
				// PRESCALE_T0 es un valor entero que determina cada cuantos ciclos de clock se incrementa T0TC
				// Se resta 1 para que vaya desde 0 hasta PRESCALE_T0-1
        // e.g. Si PRESCALE_T0 = 60000, entonces clock cycles @60Mhz = 1 mS

    //setReg32(T0MR0 , DELAY_MS_T0-1);
				// T0MR0 determina que se matchee contra el Match Register 0
				// DELAY_MS_T0 es un valor entero que determina cada cuantas cuentas se matchea el T0TC
				// Se resta 1 para que vaya desde 0 hasta DELAY_MS_T0-1
				// e.g. Si DELAY_MS_T0 = 500 y PRESCALE_T0= 60000, entonces matchea cada 500ms, o sea 0.5s
				   
    //setReg32(T0MCR , (MR0I | MR0R) );
		    // Se establece que MR0, y solo MR0, interrumpe y resetea el TC cuando matchea

    //----------Setup Timer0 Interrupt-------------
 //   VIC_EnableIRQ(TIMER0_IRQ, TIMER0_IRQ, (uint32_t*)INTERRUPT_TIMER0, false);
		 
		setReg32(VICVectAddr4 , (unsigned )INTERRUPT_TIMER0 );	// antes estaba :VICVectAddr4 = (unsigned )T0ISR; //Pointer Interrupt Function (ISR)
		setReg32(VICVectCntl4 , 0x00000020 | 0x00000004);				//0x20 (bit[5] = 1) -> to enable Vectored IRQ slot
																														//0x04 (bits[4:0]) -> this the source number - 0x04 es el valor para la interrupcion del Timer 0
																														//You can get the VIC Channel number from Lpc214x manual R2 - pg 58 / sec 5.5
 
	// VICVect info http://www.ocfreaks.com/lpc2148-interrupt-tutorial/
		setReg32(VICIntEnable,VICIntEnable |= 0x000000010);		//Habilita interrupcion Timer0
		setReg32(T0TCR,0x00000002);		// Resetea contador y prescaler
		setReg32(T0TCR,0x00000001);		// Habilita contador y prescaler

	
	// ###########################################################
	
}


void init_T1(void){
	
    setReg32(T1CTCR , 0x00000000);			// Frena el Timer1
    setReg32(T1PR , 0x00000002);				// Frecuencia de clock: 30MHz
    setReg32(T1MR0 , 0x2710);						// Setea el matheo cada 0x2710 cuentas
    setReg32(T1MCR , (MR0I | MR0R) );		// Se establece que MR0, y solo MR0, interrumpe y resetea el TC cuando matchea

		setReg32(VICVectAddr2 , (unsigned )INTERRUPT_TIMER1 );	// antes estaba :VICVectAddr2 = (unsigned )T1ISR; //Pointer Interrupt Function (ISR)
		setReg32(VICVectCntl2 , 0x00000020 | 5 );	//0x20 (i.e bit5 = 1) -> to enable Vectored IRQ slot
																						//5 (bit[4:0]) -> this the source number - here its timer0 which has VIC channel mask # as 4
																						//You can get the VIC Channel number from Lpc214x manual R2 - pg 58 / sec 5.5
 	// VICVect info http://www.ocfreaks.com/lpc2148-interrupt-tutorial/
		setReg32(VICIntEnable,VICIntEnable |= 0x000000020);		//Habilita interrupcion Timer1
		setReg32(T1TCR,0x00000002);		// Resetea contador y prescaler
		setReg32(T1TCR,0x00000001);		// Sincroniza y habilita contador y prescaler
		VIC_EnableIRQ(TIMER1_IRQn, 1, (uint32_t*)INTERRUPT_TIMER1, false);
	// ###########################################################
}


__weak void INTERRUPT_TIMER1 (void) __irq
{
	
}
__weak void INTERRUPT_TIMER0 (void) __irq
{
	
}
