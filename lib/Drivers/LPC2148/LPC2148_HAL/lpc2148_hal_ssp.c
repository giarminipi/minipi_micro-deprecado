/**
  ******************************************************************************
  * @file    lpc2148_hal_ssp.c
  * @author  Pablo D.Folino y Sergio Alberino
  * @version 1.0
  * @date    12/05/2015
  * @brief   SSP Hardware Abstraction Layer (HAL)
  ******************************************************************************
  * @attention
  *
  ******************************************************************************
	*/

#include <lpc2148_hal.h>
#include <limits.h>

const char *SSP_status[6] = {
	"SSP OK",
	"Tx Buffer Full",
	"Tx Buffer Empty",
	"Rx Buffer FULL",
	"Rx Buffer Empty",
	"SSP Busy"
};



static void freqcalc (uint32_t freq, uint8_t* scr, uint8_t* cpsdvsr)
{
	// FREQ_SPP = PCLK / [CPSDVSR * (SCR+1)]

	// Obtengo el divisor final
	uint32_t pscl = perClk()/freq;

	// Busco la convinación que menor error me genera en la obtención de la frecuencia final
	uint8_t min_div = pscl/0x100 ; 										//Comenzamos con el mínimo valor posible ==> el máximo divisor de (SCR+1) ==> (0xFF+1)= 0x100
	
	// Si el valor es cero o la división tuvo resto le aumento uno
	if (min_div > 0 || pscl%0x100 != 0)
		min_div++;
	
	//Si me dio impar le sumo uno para comenzar con un número par mayor al anterior
	min_div += min_div & 0x01;

	// Busco los divisores
	uint8_t min_reminder = UINT8_MAX;
	for (uint16_t i = min_div; i<0xFF & min_reminder!=0; i=i+2)
		if ( pscl%i < min_reminder)
		{
			min_div = i;
			min_reminder = pscl%i;
		}
	
	*scr = pscl/min_div - 1;
	*cpsdvsr = min_div;
}

SSP_Status initSSP(SSP_Modes mode, SSP_FORMAT format, SSP_CLK_Pol clkpol, SSP_PHASE phase, SSP_TxBits num_bits, int ssp_freq, int IRQ_Masks, int IRQ_priority)
{
	// Get Peripherial Frequency
	uint8_t scr, cpsdvsr;
	freqcalc(ssp_freq, &scr, &cpsdvsr);

	// Configure SSP
	SSPCR0 = scr << 8 | (int) phase << 7 | (int) clkpol << 6 | (int) format << 4 | (int) num_bits;
	SSPCR1 = (int) mode << 2;
	SSPCPSR = cpsdvsr;

	// Setup IRQ
	if (IRQ_Masks)
	{
		VIC_EnableIRQ(SSP_IRQn, IRQ_priority, (uint32_t*) SSP_IRQ, false);
		SSPIMSC = IRQ_Masks & 0x0F;
	}

	return SSP_OK;
}

SSP_Status deinitSSP(void)
{
	// Disable IRQ
	VIC_DisableIRQ(SSP_IRQn);
	
	// Clear Interrupts
	SSPICR = 0;
	
	// Remove Interrupt Masks
	SSPIMSC = 0x00;

	// Reset SSP Control Register 0
	SSPCR0 = 0;

	// Reset SSP Control Register 1
	SSPCR1 = 0;

	// Reset Clock Prescale Register
	SSPCPSR = 0;
	
	// Return no errors
	return SSP_OK;
}

int stateSSP (void)
{
	// Read the status of the SSP (0xF8 because of the reserved bits)
	return SSPSR & 0x1F;
}

SSP_Status writeSSP (int data)
{
	if (IS_SSP_TxFIFO_Not_Full())
	{
		SSPDR = data;
		return SSP_OK;
	}

	return SSP_Error;
}

SSP_Status readSSP (uint16_t *data)
{
	if (IS_SSP_RxFIFO_Not_Empty())
	{
		*data = (uint16_t) SSPDR;
		return SSP_OK;
	}

	return SSP_Error;
}

SSP_Status SSP_Enable (bool state)
{
	if (state)
		SSPCR1 |= 0x02;
	else
		SSPCR1 &= 0xFD;

	return SSP_OK;
}

SSP_Status SSP_LoopBack_Enable (bool state)
{
	if (state)
		SSPCR1 |= 0x01;
	else
		SSPCR1 &= 0xFE;

	return SSP_OK;
}

__weak __irq void SSP_IRQ (void)
{
	//---- Must be filled by the user ----
	
	
	//---- Remenver to acknowledge the ISR when it has finished execution ----
	//VICVectAddr = 0;
}






///*
// ==================================================================================================
// Nombre: 	  SSI.c
// Autores: 	Juan Manuel Perdomo
// Emails:		JuMaPerdomo@gmail.com
// Creado:	  28/08/2012
// Versi�n: 	1.0
// Copyright: GNU
// Estado:
// ==================================================================================================
// Descripci�n
// ==================================================================================================
// -   Funciones  relacionadas con la cuminicacion SSI											                        -
// -                                                                                                -
// -                                                                                                -
// -                                                                                                -
// ==================================================================================================
// */
////-------------------------------------------------------------------------------------------------
////                                      			  Librerias                  	  	                  -
////-------------------------------------------------------------------------------------------------
//#include <lpc2148_hal.h>

////-------------------------------------------------------------------------------------------------
////                                      			Definiciones                  	  	                -
////-------------------------------------------------------------------------------------------------
//#define	FIFOSIZE		8
//#define SSPDataBits 16

///// El modo de funcionamiento mas cercano para el sensor es el
////  SSI con formato SPI y los flags CPOL=1 y CPHA=1

//// SSPSR   Definiciones a nivel Bit
//#define TFE		0
//#define TNF		1
//#define RNE		2
//#define RFF		3
//#define BSY		4

////SSPIMSC / SSPRIS / SSPMIS  Definiciones a nivel Bit
//#define RORIM		0
//#define RTIM		1
//#define RXIM		2
//#define TXIM		3

////SSPICR
//#define RORIC		0
//#define RTIC		1

//// SSPCR1   Definiciones a nivel Bit
//#define LBM		    0
//#define SSE    		1
//#define MSTR			2

//// SSPCR0   Definiciones a nivel Bit
//#define DDS 			0						// Bits: 0, 1, 2 y 3
//#define FRF 			4		 			 	// Bits: 4 y 5
//#define CPOL    	6
//#define CPHA    	7
//#define SCR				8						// Bits: 8, 9, 10, 11, 12 ,13, 14 y 15
//// -----------------------------------------------------------------------------------------------
////                                            Bauer
//// -----------------------------------------------------------------------------------------------
///// Registro que controla la direccion de los pines de SSI
//#define SSI_IODIR      			IODIR0
//#define SSI_I1DIR      			IODIR1
//#define SSI_SCK_PIN    			30			/* Clock       P0.30  out */
//#define SSI_MISO_PIN   			29			/* Desde la placa   P0.29  in  */
////#define SSI_MOSI_PIN   		22		  /* A la placa   (sin implmentar)    out   - NO SE USA */
//#define SSI_SS_PIN	   			19			/* Se�al de Select P1.19 - GPIO out */
//#define SSI_SS_2_PIN   			16			/* Se�al de Select P1.16 - GPIO out */
////#define SSI_PROG_PIN			17			/* Se�al de Programacion P0.16 - GPIO out - NO SE USA */

///// Registro que controla la funcion de los pines de SSI
//#define SSI_PINSEL_1     		PINSEL1
//#define SSI_SCK_FUNCBIT   	28  			// Bits: 2 y 3
//#define SSI_MISO_FUNCBIT  	26				// Bits: 4 y 5
////#define SSI_PROG_FUNCBIT	0					// Bits: 0 y 1

//#define SSI_PINSEL_0     		PINSEL0
////#define SSI_MOSI_FUNCBIT  	24				// Bits: 25 y 24

//#define SSI_PRESCALE_REG  	SSPCPSR    	// de 2 a 254

////// -----------------------------------------------------------------------------------------------
//////                                            OLD
//////// -----------------------------------------------------------------------------------------------
/////// Registro que controla la direccion de los pines de SSI
////#define SSI_IODIR      			IODIR0
////#define SSI_SCK_PIN    			17			/* Clock       P0.17  out */
////#define SSI_MISO_PIN   			18			/* Desde la placa   P0.18  in  */
////#define SSI_MOSI_PIN   			19			/* A la placa     P0.19  out */
////#define SSI_SS_PIN	   			20			/* Se�al de Select P0.20 - GPIO out */
////#define SSI_SS_2_PIN   			22			/* Se�al de Select P0.22 - GPIO out */
////#define SSI_PROG_PIN				16			/* Se�al de Programacion P0.16 - GPIO out */
////// -----------------------------------------------------------------------------------------------

/////// Registro que controla la funcion de los pines de SSI
////#define SSI_PINSEL     			PINSEL1
////#define SSI_SCK_FUNCBIT   	3  			 		//  Bits: 2 y 3
////#define SSI_MISO_FUNCBIT  	5 					// 	 Bits: 4 y 5
////#define SSI_MOSI_FUNCBIT  	7					//  Bits: 6 y 7
////#define SSI_SS_FUNCBIT    	9					// 	 Bits:8 y 9
////#define SSI_SS_2_FUNCBIT   	13					// 	 Bits:12 y 13
////#define SSI_PROG_FUNCBIT		0					// Bits: 0 y 1

////#define SSI_PRESCALE_REG  	SSPCPSR    	// de 2 a 254
////-------------------------------------------------------------------------------------------------
////                                    Macros Para el SSI "Manual"                	  	            -
////-------------------------------------------------------------------------------------------------

//#define 	ClearBit_P0(x) 		IOCLR0 = (1 << x)
//#define 	SetBit_P0(x) 			IOSET0 = (1 << x)
//#define   ReadBit_P0(x)			(IOPIN0 &(1 << x))

//#define   Clear_CSN(x)   		ClearBit_P0(x)
//#define   Set_CSN(x)   			SetBit_P0(x)

//#define		Clear_CLK 				ClearBit_P0(SSI_SCK_PIN )
//#define   Set_CLK   				SetBit_P0( SSI_SCK_PIN )

//#define		Read_DO						ReadBit_P0(SSI_MISO_PIN)

//#define		Clear_Prog 				ClearBit_P0(SSI_PROG_PIN )
//#define   Set_Prog   				SetBit_P0( SSI_PROG_PIN )

////-------------------------------------------------------------------------------------------------
////                                    Variable Global                           	  	            -
////-------------------------------------------------------------------------------------------------

////=================================================================================================
////										                          Funciones
////=================================================================================================
///*__________________________________________________________________________________
// |																													                        |
// |  Nombre: 	Wait_SSI																							                |
// |	Accion:  	Delay temporal - Wait_SSI(1) simboliza medio pulso de clock de				|
// |		 	 	    transmicion 																						              |
// |																															                    |
// |	Recive:	 	uint16_t mul    Cantidad de pulsos de delay								      		  |
// |																															                    |
// |	Devuelve:  Nada																									                |
// |__________________________________________________________________________________|
// */
//void Wait_SSI(uint16_t mul)
//{
//	uint16_t i = 0x00;
//	for (i = 0x00; i < 1 * mul; i++)
//	{
//	}

//}

///*__________________________________________________________________________________
// |																													                        |
// | Nombre: 	SSIManualINIT																					                  |
// |	Accion:  	Setea los puertos que usa la comuicacion SSI										      |
// |																															                    |
// |	Recive:	 	Nada																									                |
// |																															                    |
// |	Devuelve:  Nada																									                |
// |__________________________________________________________________________________|
// */
//void SSIManualINIT(void)
//{
//// Direcciones
//	// OUT : 	"1"
//	SSI_IODIR |= (1 << SSI_SCK_PIN) ;
//	SSI_I1DIR |= (1 << SSI_SS_PIN) | (1 << SSI_SS_2_PIN);
//	// IN : 	"0"
//	SSI_IODIR &= ~((1 << SSI_MISO_PIN));
//
//	Set_CSN(SSI_SS_PIN);  // Pongo en alto el chip select 1
//	Set_CSN(SSI_SS_2_PIN); 	// Pongo en alto el chip select 2
//}

///*__________________________________________________________________________________
// |																													                        |
// | Nombre: 	SSIManualRead																					                  |
// |	Accion:  	Setea los puertos que usa la comuicacion SSI										      |
// |																															                    |
// |	Recive:	 																											                  |
// |					uint8_t * DataINBuff	 Puntero sonde se guardan los bits de la trama		|
// |					uint8_t Nbits				Cantidad de bits de la trama								        |
// |																															                    |
// |	Devuelve:  Nada																									                |
// |__________________________________________________________________________________|
// */

//uint8_t SSI_Cs_Select(uint8_t Cs_Device)
//{
//  switch (Cs_Device)
//    {
//    case 1:
//      return (SSI_SS_PIN);
//    case 2:
//      return (SSI_SS_2_PIN);
//    default:
//      return (SSI_SS_PIN);
//    }
//}

///*__________________________________________________________________________________
// |																													                        |
// | Nombre: 	SSIManualRead																					                  |
// |	Accion:  	Setea los puertos que usa la comuicacion SSI										      |
// |																															                    |
// |	Recive:	 																											                  |
// |					uint8_t * DataINBuff	 Puntero sonde se guardan los bits de la trama		|
// |					uint8_t Nbits				Cantidad de bits de la trama								        |
// |																															                    |
// |	Devuelve:  Nada																									                |
// |__________________________________________________________________________________|
// */
//void SSIManualRead(uint8_t * DataINBuff, uint8_t Nbits, uint8_t CS_PIN)
//{
//	uint8_t Bit_Actual = 0x00;
//	uint8_t Bite_Rcv = 0x00;

//	Clear_CSN(CS_PIN);
//	Wait_SSI(5);

//	Clear_CLK;
//	Wait_SSI(1);

//	for (Bit_Actual = Nbits; Bit_Actual >> 0; Bit_Actual--)
//	{
//		Set_CLK;
//		Wait_SSI(1);

//		Bite_Rcv += (Read_DO) ? 1 : 0;

//		Clear_CLK;
//		Wait_SSI(1);

//		if (((Bit_Actual - 1) & 0x07) == 0)
//		{
//			DataINBuff[Bit_Actual >> 3] = Bite_Rcv;
//			Bite_Rcv = 0;
//		}
//		Bite_Rcv <<= 1;
//	}
//	Set_CLK;
//	Set_CSN(CS_PIN);

//}

///*__________________________________________________________________________________
// |																													                        |
// |  // No verificada//																						       	          |
// |																													       	                |
// | Nombre: 	OneTime_writeOTP																			                  |
// |	Accion:  	Setea los puertos que usa la comuicacion SSI										      |
// |																															                    |
// |	Recive:	 																											                  |
// |					uint8_t * DataOutBuff	 Puntero sonde se guardan los bits de la trama		|
// |					uint8_t Nbits				Cantidad de bits de la trama								        |
// |																															                    |
// |	Devuelve:  Nada																									                |
// |																															                    |
// |	Nota Importante: 	Solo se puede mandar una ves antes de operar esta palabra 	 	|
// |	 	 	 	 	 	 	 	de programacion, mandarla de nuevo causara que el sensor 	 	      |
// |	 	 	 	 	 	 	 	se cuelgue y sea necesario apagarlo para que vuelva a  	 		      |
// |	 	 	 	 	 	 	 	funcionar correctamente. 	 													              |
// |	Nota : 					Esta palabra OTP de borra una vez que se resetea el micro y   	|
// |	 	 	 	 	 	 	 	vuelve a la anterior grabada  	 												          |
// |__________________________________________________________________________________|
// */
////void OneTime_writeOTP(uint8_t* DataOutBuff, uint8_t Nbits, uint8_t CS_PIN)
////{
////	uint8_t Bit_Actual = 0x00;
////	uint8_t * Pt_Byte_actual = 0x00;

////	uint8_t Out_Byte = 0;

////	Pt_Byte_actual= DataOutBuff + ((Nbits - 1) >> 3);	///8 ;
////	Out_Byte= *Pt_Byte_actual;

////	if (Nbits % 8)
////	{
////		Out_Byte <<= 8 - (Nbits % 8);
////	}


////// Se�ales de inicializacion de escritura del registro OTP
////	Clear_Prog;
////	Clear_CSN(CS_PIN);
////	Wait_SSI(500);
////	Clear_CLK;
////	Wait_SSI(500);
////	Set_Prog;
////	Wait_SSI(500);
////	Set_CSN(CS_PIN);
////	Wait_SSI(500);
////// Todo: Revisar en un OSC
//////-- Enviar Data al OTP
////	for (Bit_Actual = Nbits; Bit_Actual  ; Bit_Actual--)
////	{

////		if (Out_Byte& 0x80)
////		{
////			Set_Prog;
////			Wait_SSI(600);
////		}
////		else
////		{
////			Clear_Prog;
////			Wait_SSI(600);
////		}

////		Set_CLK;
////		Wait_SSI(300);	// delay
////		Clear_CLK;
////		Wait_SSI(50);

////		Out_Byte<<= 1;
////		if (((Bit_Actual- 1) & 0x07) == 0)
////		{
////			Out_Byte= *(--Pt_Byte_actual);
////		}
////	}

////	// Se�ales de finde escritura del registro OTP
////	Clear_Prog; // set PROG_OUT=0
////	Wait_SSI(600);
////	Clear_CSN(CS_PIN); //set CSN=0
////	Wait_SSI(600);
////	Set_CSN(CS_PIN); //set CSN=1
////	Wait_SSI(600);
////	Set_CLK;
////	Wait_SSI(100);
////}
