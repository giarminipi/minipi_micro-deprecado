/**
  ******************************************************************************
  * @file    lpc2148_hal_vic.c
  * @author  
  * @version 
  * @date    
  * @brief   
  ******************************************************************************
  * @attention
  *
  ******************************************************************************
	*/

#include <lpc2148_hal.h>
#include <assert.h>


//---------------------------------------------------------------------
//									VECTOR INTERRUPT CONTROLER FUNCTIONS
//---------------------------------------------------------------------


void VIC_EnableIRQ(IRQn_Type IRQn, uint32_t priority, uint32_t* function_addr, bool set_as_FIRQ)
{
	// Priority cannot be greater than 15, unless is in Default VIC
	assert(priority < 16);
	
	// Set VIC Priority (Slot of the IRQ)
	volatile unsigned long* VICVectCntlX = &VICVectCntl0;
	VICVectCntlX += priority;
	*VICVectCntlX = 0x20 | IRQn ;
	
	// Set ISR Address
	volatile unsigned long* VICVectAddrX = &VICVectAddr0;
	VICVectAddrX += priority;
	*VICVectAddrX = (uint32_t) function_addr;
	
	// Set as Fast or Normal
	if(set_as_FIRQ)
	{
		assert(VICIntSelect != 0);
		VICIntSelect |= (1<<IRQn);
	}
	else
	{
		VICIntSelect &= ~(1<<IRQn);
	}
	
	// Enable Interrupt
	VICIntEnable |= (1<<IRQn);
}

void VIC_DisableIRQ(IRQn_Type IRQn)
{
	// Disable Interrupt
	VICIntEnClr |= (1<<IRQn);
}

uint32_t VIC_GetPendingIRQ (IRQn_Status_Type status_type)
{
	uint32_t status;
	
	if (status_type == All_IRQs)
		status = VICRawIntr;
	
	if (status_type == Enable_and_Normal_IRQs)
		status = VICIRQStatus;
	
	if (status_type == Enable_and_Fast_IRQs)
		status = VICFIQStatus;

	return status;
}

bool VIC_GetActive (IRQn_Type IRQn)
{
	return VICIRQStatus && (1<<IRQn);
}

uint32_t VIC_GetPriority (IRQn_Type IRQn)
{
	int i;
	volatile unsigned long* VICVectCntlX = &VICVectCntl0;
	
	for(i=0; i<16; i++)
		if( IRQn == *(VICVectCntlX + i) )
			break;
	
	return i;
}
