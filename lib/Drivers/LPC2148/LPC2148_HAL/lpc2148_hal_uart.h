/**
  ******************************************************************************
  * @file    lpc2148_hal_uart.h
  * @author
  * @version
  * @date
  * @brief
  ******************************************************************************
  * @attention
  *
  ******************************************************************************
	*/

#ifndef _LPC2148_HAL_UART_H
#define _LPC2148_HAL_UART_H

#include <lpc2148_hal_def.h>
#include <stdbool.h>

//#define _UART0


#if defined ( __CC_ARM   )
#pragma anon_unions
#endif

typedef enum
{
	UART0 = 0,
	UART1
} UARTx;

typedef struct
{
    union
    {
		__I  uint8_t  RBR;
		__O  uint8_t  THR;
		__IO uint8_t  DLL;
    };
		uint8_t RESERVED0[3];
    union
    {
		__IO uint8_t DLM;
		__IO uint8_t IER;
    };
		uint8_t RESERVED1[3];
    union
    {
		__I  uint8_t IIR;
		__O  uint8_t FCR;
    };
		uint8_t  RESERVED2[3];
    __IO uint8_t  LCR;
    uint8_t  RESERVED3[3];
    __IO uint8_t  MCR;
    uint8_t  RESERVED4[3];
    __I  uint8_t  LSR;
    uint8_t  RESERVED5[3];
    __I  uint8_t  MSR;
    uint8_t  RESERVED6[3];
    __IO uint8_t  SCR;
    uint8_t  RESERVED7[3];
    __IO uint32_t ACR;
    uint8_t  RESERVED8[4];
    __IO uint32_t  FDR;
    uint8_t  RESERVED9[4];
    __IO uint8_t  TER;
//    uint8_t  RESERVED8[27];
//    __IO uint8_t  RS485CTRL;
//    uint8_t  RESERVED9[3];
//    __IO uint8_t  ADRMATCH;
//    uint8_t  RESERVED10[3];
//    __IO uint8_t  RS485DLY;
//    uint8_t  RESERVED11[3];
//    __I  uint8_t  FIFOLVL;				//no hay referencia en la hoja de datos
} LPC_UART_Regs;					//revisar guia de usuario



/************************** memory map ********************************/
#define LPC_VPB_BASE	0xE0000000UL
#define	LPC_UART0_BASE	(LPC_VPB_BASE + 0x0C000)
#define	LPC_UART1_BASE	(LPC_VPB_BASE + 0x10000)
#define LPC_SC_BASE		(LPC_VPB_BASE + 0x1FC000)


/* Public Macros -------------------------------------------------------------- */
#define UART_BLOCKING_TIMEOUT	 (0xFFFFFFFFUL)

/* Private Macros ------------------------------------------------------------- */
/* Accepted Error baud rate value (in percent unit) */
#define UART_ACCEPTED_BAUDRATE_ERROR    (3)

/* --------------------- BIT DEFINITIONS -------------------------------------- */
/*********************************************************************/
#define UART_RBR_MASKBIT		((uint8_t)0xFF)
/*********************************************************************/
#define UART_THR_MASKBIT		((uint8_t)0xFF)
/*********************************************************************/
#define UART_DLL_MASKBIT		((uint8_t)0xFF)
#define UART_LOAD_DLL(div)		((div) & UART_DLL_MASKBIT)
/*********************************************************************/
#define UART_DLM_MASKBIT		((uint8_t)0xFF)
#define UART_LOAD_DLM(div)		((div >> 8) & UART_DLM_MASKBIT)
/*********************************************************************/
//Bits del UxIER
#define UART_IER_RBRINT_EN		((uint32_t)(1<<0))
#define UART_IER_THREINT_EN		((uint32_t)(1<<1))
#define UART_IER_RLSINT_EN		((uint32_t)(1<<2))
#define UART1_IER_MSINT_EN		((uint32_t)(1<<3))
#define UART1_IER_CTSINT_EN		((uint32_t)(1<<7))
#define UART_IER_ABEOINT_EN		((uint32_t)(1<<8))
#define UART_IER_ABTOINT_EN		((uint32_t)(1<<9))

#define UART1_IER_BITMASK		((uint32_t)(0x307))		//0..001100000111
//#define UART1_IER_BITMASK		((uint32_t)(0x38F))		//0..001110001111

/*********************************************************************/

//Bits del UxIIR
#define UART_IIR_INTSTAT_PEND   ((uint32_t)(1<<0))
#define UART_IIR_INTID_RLS		((uint32_t)(3<<1))
#define UART_IIR_INTID_RDA		((uint32_t)(2<<1))
#define UART_IIR_INTID_CTI		((uint32_t)(6<<1))
#define UART_IIR_INTID_THRE		((uint32_t)(1<<1))
#define UART1_IIR_INTID_MODEM   ((uint32_t)(0<<1))
#define UART_IIR_INTID_MASK		((uint32_t)(7<<1))
#define UART_IIR_FIFO_EN		((uint32_t)(3<<6))
#define UART_IIR_ABEO_INT		((uint32_t)(1<<8))
#define UART_IIR_ABTO_INT		((uint32_t)(1<<9))

#define UART_IIR_BITMASK		((uint32_t)(0x3CF))		//0..001111001111

/*********************************************************************/
#define UART_FCR_FIFO_EN		((uint8_t)(1<<0))
#define UART_FCR_RX_RS			((uint8_t)(1<<1))
#define UART_FCR_TX_RS			((uint8_t)(1<<2))
//#define UART_FCR_DMAMODE_SEL   				((uint8_t)(1<<3))		//el lpc2148 no tiene DMA mode
#define UART_FCR_TRG_LEV0		((uint8_t)(0))
#define UART_FCR_TRG_LEV1		((uint8_t)(1<<6))
#define UART_FCR_TRG_LEV2		((uint8_t)(2<<6))
#define UART_FCR_TRG_LEV3		((uint8_t)(3<<6))
//entonces
#define UART_FCR_BITMASK		((uint8_t)(0xC7))			//11000111
#define UART_TX_FIFO_SIZE		(16)									//revisar hdd  lp1700 (16)

/*********************************************************************/
#define UART_LCR_WLEN5			((uint8_t)(0))
#define UART_LCR_WLEN6			((uint8_t)(1<<0))
#define UART_LCR_WLEN7			((uint8_t)(2<<0))
#define UART_LCR_WLEN8			((uint8_t)(3<<0))
#define UART_LCR_STOPBIT_SEL	((uint8_t)(1<<2))
#define UART_LCR_PARITY_EN		((uint8_t)(1<<3))
#define UART_LCR_PARITY_ODD		((uint8_t)(0))
#define UART_LCR_PARITY_EVEN	((uint8_t)(1<<4))
#define UART_LCR_PARITY_F_1		((uint8_t)(2<<4))
#define UART_LCR_PARITY_F_0		((uint8_t)(3<<4))
#define UART_LCR_BREAK_EN		((uint8_t)(1<<6))
#define UART_LCR_DLAB_EN		((uint8_t)(1<<7))

#define UART_LCR_BITMASK		((uint8_t)(0xFF))    //ok
/**************************************************************falta*******/
#define UART1_MCR_DTR_CTRL		((uint8_t)(1<<0))
#define UART1_MCR_RTS_CTRL		((uint8_t)(1<<1))
#define UART1_MCR_LOOPB_EN		((uint8_t)(1<<4))
#define UART1_MCR_AUTO_RTS_EN	((uint8_t)(1<<6))
#define UART1_MCR_AUTO_CTS_EN	((uint8_t)(1<<7))

#define UART1_MCR_BITMASK		((uint8_t)(0x0F3))
/***********************************************************<-*******/

//Bits del ULSR
#define UART_LSR_RDR			((uint8_t)(1<<0))
#define UART_LSR_OE				((uint8_t)(1<<1))
#define UART_LSR_PE				((uint8_t)(1<<2))
#define UART_LSR_FE				((uint8_t)(1<<3))
#define UART_LSR_BI				((uint8_t)(1<<4))
#define UART_LSR_THRE			((uint8_t)(1<<5))
#define UART_LSR_TEMT			((uint8_t)(1<<6))
#define UART_LSR_RXFE			((uint8_t)(1<<7))

#define UART_LSR_BITMASK		((uint8_t)(0xFF))			//ok

/**************************************revisar***->****************************/
#define UART1_MSR_DELTA_CTS		((uint8_t)(1<<0))
#define UART1_MSR_DELTA_DSR		((uint8_t)(1<<1))
#define UART1_MSR_LO2HI_RI		((uint8_t)(1<<2))
#define UART1_MSR_DELTA_DCD		((uint8_t)(1<<3))
#define UART1_MSR_CTS			((uint8_t)(1<<4))
#define UART1_MSR_DSR			((uint8_t)(1<<5))
#define UART1_MSR_RI			((uint8_t)(1<<6))
#define UART1_MSR_DCD			((uint8_t)(1<<7))

#define UART1_MSR_BITMASK		((uint8_t)(0xFF))
/*********************************************************************/
#define UART_SCR_BIMASK			((uint8_t)(0xFF))			//ok
/*********************************************************************/
//Auto-baudrate Control Register
#define UART_ACR_START			((uint32_t)(1<<0))
#define UART_ACR_MODE			((uint32_t)(1<<1))
#define UART_ACR_AUTO_RESTART	((uint32_t)(1<<2))
#define UART_ACR_ABEOINT_CLR	((uint32_t)(1<<8))
#define UART_ACR_ABTOINT_CLR	((uint32_t)(1<<9))

#define UART_ACR_BITMASK		((uint32_t)(0x307))   //0..001100000111

/*********************************************************************/
//Fractional Divider Register
//nota: revisar formula de calculo
#define UART_FDR_DIVADDVAL(n)	((uint32_t)(n&0x0F))
#define UART_FDR_MULVAL(n)		((uint32_t)((n<<4)&0xF0))

#define UART_FDR_BITMASK		((uint32_t)(0xFF))

/*********************************************************************/
//Transmit Enable Register
#define UART_TER_TXEN			((uint8_t)(1<<7))

#define UART_TER_BITMASK		((uint8_t)(0x80))

/*********************************************************************/
// Modem Register
#define UART1_RS485CTRL_NMM_EN		((uint32_t)(1<<0))
#define UART1_RS485CTRL_RX_DIS		((uint32_t)(1<<1))
#define UART1_RS485CTRL_AADEN		((uint32_t)(1<<2))
#define UART1_RS485CTRL_SEL_DTR		((uint32_t)(1<<3))
#define UART1_RS485CTRL_DCTRL_EN	((uint32_t)(1<<4))
#define UART1_RS485CTRL_OINV_1		((uint32_t)(1<<5))

#define UART1_RS485CTRL_BITMASK		((uint32_t)(0x3F))

/*********************************************************************/
#define UART1_RS485ADRMATCH_BITMASK ((uint8_t)(0xFF))

/*********************************************************************/
/* Macro defines for UART1 RS-485 Delay value register */
#define UART1_RS485DLY_BITMASK		((uint8_t)(0xFF))
/*********************************************************************/
//no hay referencia en el datasheet
//#define UART_FIFOLVL_RXFIFOLVL(n)			((uint32_t)(n&0x0F))
//#define UART_FIFOLVL_TXFIFOLVL(n)			((uint32_t)((n>>8)&0x0F))

//#define UART_FIFOLVL_BITMASK			((uint32_t)(0x0F0F))
/*****************************************************<-***revisar*************/

/*********************************************************************************************/
//	UART Line Control Register
//	<1:0>	Word Length Select
//	<2>		Stop bits
//	<3>		Parity enable		Si es 0 deshabiliata paridad
//	<5:4>	Parity Select	00 impar
//	<6>		Break control	Si es 0 esta deshabilitado
//	<7>		Divisor Latch Access Bit	si es 0 NO permite acceso a UDLL y UDLM

//	UFCR:	Controla las FIFO de transminsión y recepción
//	UART FIFO Control Register
//	Rx Trigger Level<7:6>

//	UxIER:	interrupt Enable Register
//	para acceder al UART0 Interrupt Enable Register, el bit DLAB debe estar en estado bajo
//	para acceder al UART1 Interrupt Enable Register, el bit DLAB debe estar en estado bajo
//													Modem status
//													CTS

//UART Interrupt Identification Register
//UxIIR:	Proporciona el estado e indica la prioridad y fuente de una interrupción pendiente

//	ULSR:	Registro de solo lectura, entrega información sobre el estado de transmisión y recepción
//	UART Line Status Registor


/*********************************************************************************************/
// Configuración de la UART

/**	@enum UART_DATABIT_Type 
	* @brief Word Length Select
*/
typedef enum
{
    UART_DATABIT_5 = UART_LCR_WLEN5,
    UART_DATABIT_6 = UART_LCR_WLEN6,
    UART_DATABIT_7 = UART_LCR_WLEN7,
    UART_DATABIT_8 = UART_LCR_WLEN8
} UART_DATABIT_Type;

//	Stop bits
typedef enum
{
    UART_STOPBIT_1 = 0,
    UART_STOPBIT_2 = UART_LCR_STOPBIT_SEL
} UART_STOPBIT_Type;

//	Parity
typedef enum
{
    UART_PARITY_NONE = 0,
    UART_PARITY_ODD  = UART_LCR_PARITY_EN | UART_LCR_PARITY_ODD,
    UART_PARITY_EVEN = UART_LCR_PARITY_EN | UART_LCR_PARITY_EVEN,
    UART_PARITY_SP_1 = UART_LCR_PARITY_EN | UART_LCR_PARITY_F_1,
    UART_PARITY_SP_0 = UART_LCR_PARITY_EN | UART_LCR_PARITY_F_0
} UART_PARITY_Type;

// FIFO Trigger Level
typedef enum
{
	UART_FIFO_RX_Disable = 0,
	UART_FIFO_RX_TRGLEV0 = UART_FCR_FIFO_EN | UART_FCR_TRG_LEV0,
	UART_FIFO_RX_TRGLEV1 = UART_FCR_FIFO_EN | UART_FCR_TRG_LEV1,
	UART_FIFO_RX_TRGLEV2 = UART_FCR_FIFO_EN | UART_FCR_TRG_LEV2,
	UART_FIFO_RX_TRGLEV3 = UART_FCR_FIFO_EN | UART_FCR_TRG_LEV3
} UART_FIFO_Type;

// Interrupt Type
typedef enum
{
	UART_INTCFG_NONE = 0,
	UART_INTCFG_RBR  = UART_IER_RBRINT_EN,
	UART_INTCFG_THRE = UART_IER_THREINT_EN,
	UART_INTCFG_RLS  = UART_IER_RLSINT_EN,
	UART1_INTCFG_MS  = UART1_IER_MSINT_EN,
	UART1_INTCFG_CTS = UART1_IER_CTSINT_EN,
	UART_INTCFG_ABEO = UART_IER_ABEOINT_EN,
	UART_INTCFG_ABTO = UART_IER_ABTOINT_EN
} UART_INT_Type;

typedef enum
{
	UART_LINESTAT_RDR  = UART_LSR_RDR,
	UART_LINESTAT_OE   = UART_LSR_OE,
	UART_LINESTAT_PE   = UART_LSR_PE,
	UART_LINESTAT_FE   = UART_LSR_FE,
	UART_LINESTAT_BI   = UART_LSR_BI,
	UART_LINESTAT_THRE = UART_LSR_THRE,
	UART_LINESTAT_TEMT = UART_LSR_TEMT,
	UART_LINESTAT_RXFE = UART_LSR_RXFE
} UART_LS_Type;


typedef enum
{
	UART_AUTOBAUD_INTSTAT_ABEO = UART_IIR_ABEO_INT,
	UART_AUTOBAUD_INTSTAT_ABTO = UART_IIR_ABTO_INT
} UART_ABEO_Type;


typedef enum
{
	UART_AUTOBAUD_MODE0 = 0,
	UART_AUTOBAUD_MODE1,
} UART_AB_MODE_Type;


typedef struct
{
    UART_AB_MODE_Type	ABMode;
} UART_AB_CFG_Type;


typedef enum
{
    INACTIVE = 0,
    ACTIVE = !INACTIVE
} UART1_SignalState;


// UART Configuration
typedef struct
{
	uint32_t 					baud_rate;
	UART_PARITY_Type 	parity;
	UART_DATABIT_Type databits;
	UART_STOPBIT_Type stopbits;
	UART_FIFO_Type		FIFO_type;
	UART_INT_Type			INT_type;
} UART_Config;



/* Public Functions ----------------------------------------------------------- */

/* UART Init/DeInit functions --------------------------------------------------*/
void UART_ConfigStructInit ( UART_Config* UART_InitStruct, uint32_t baudrate, UART_DATABIT_Type databits, UART_PARITY_Type Parity, UART_STOPBIT_Type Stopbits, UART_FIFO_Type fifo_type, UART_INT_Type int_type );
void UART_Init ( UARTx t_uart, UART_Config *UART_Conf );
void UART_DeInit ( UARTx t_uart );

/* UART Send functions -------------------------------------------------*/
void UART_SendByte ( UARTx t_uart, uint8_t Data );
uint32_t UART_Send ( UARTx t_uart, uint8_t *txbuf, uint32_t buflen);

/* UART Receive functions -------------------------------------------------*/
uint8_t UART_ReceiveByte ( UARTx t_uart, uint8_t* data );
uint32_t UART_Receive ( UARTx t_uart, uint8_t *rxbuf, uint32_t buflen);

/* UART operate functions -------------------------------------------------------*/
void UART_IntConfig ( UARTx t_uart, UART_INT_Type int_type );
void UART_FIFOConfig ( UARTx t_uart, UART_FIFO_Type fifo_type );

void UART_Tx_Enable ( UARTx t_uart );
void UART_Tx_Disable ( UARTx t_uart );

bool UART_CheckBusy ( UARTx t_uart );
void UART_ForceBreak ( UARTx t_uart );

	
/* Private Functions ----------------------------------------------------------- */
static uint32_t uart_divisors_algorithm ( uint32_t clock, uint32_t baudrate, uint16_t* best_dlab, uint8_t* best_mulval, uint8_t* best_divval );


#endif	// End LPC2148_HAL_UART_H
