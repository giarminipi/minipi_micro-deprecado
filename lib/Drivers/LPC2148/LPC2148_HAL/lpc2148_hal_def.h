/**
  ******************************************************************************
  * @file    lpc2148_hal_def.c
  * @author  
  * @version 
  * @date    
  * @brief   
  ******************************************************************************
  * @attention
  *
  ******************************************************************************
	*/

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef _LPC2148_HAL_DEF_H
#define _LPC2148_HAL_DEF_H


/* Includes ------------------------------------------------------------------*/
#include <LPC214X.h>
#include <PE_Types.h>


#define     __I     volatile const          
#define     __O     volatile                  
#define     __IO    volatile 


#endif // End _LPC2148_HAL_DEF_H
