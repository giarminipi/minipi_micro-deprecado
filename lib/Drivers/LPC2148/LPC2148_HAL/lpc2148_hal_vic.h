/**
  ******************************************************************************
  * @file    lpc2148_hal_vic.h
  * @author  
  * @version 
  * @date    
  * @brief   
  ******************************************************************************
  * @attention
  *
  ******************************************************************************
	*/

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef _LPC2148_HAL_VIC_H
#define _LPC2148_HAL_VIC_H

/* Inludes ------------------------------------------------------------------*/
#include <lpc2148_hal_def.h>
#include <stdbool.h>

/**
  * @typedef  IRQn_Type
	* @brief	Types of IRQs
  */
typedef enum
{
	WDT_IRQn = 0,			// 0x00000000,	//bit 0
	
	ARMCORE0_IRQn = 2,	//0x00000004,		//bit 2
	ARMCORE1_IRQn = 3,	//0x00000008,		//bit 3
	
	TIMER0_IRQn = 4,		//0x00000010,		//bit 4
	TIMER1_IRQn = 5,		//0x00000020,		//bit 5
	
	UART0_IRQn = 6,		//0x00000040,		//bit 6
	UART1_IRQn = 7,		//0x00000080,		//bit 7
	
	PWM0_IRQn = 8,			//0x00000100,		//bit 8
	
	I2C0_IRQn = 9,			//0x00000200,		//bit 9
	I2C1_IRQn = 19,		//0x00080000,		//bit 19
	
	SPI0_IRQn = 10,		//0x00000400,		//bit 10
	SPI1_IRQn = 11,		//0x00000800,		//bit 11
	
	SSP_IRQn = 11,			//0x00000800,		//bit 11
	
	PLL_IRQn = 12,			//0x00001000,		//bit 12
	
	RTC_IRQn = 13,			//0x00002000,		//bit 13
	
	EINT0_IRQn = 14,		//0x00004000,		//bit 14
	EINT1_IRQn = 15,		//0x00008000,		//bit 15
	EINT2_IRQn = 16,		//0x00010000,		//bit 16
	EINT3_IRQn = 17,		//0x00020000,		//bit 17
	
	AD0_IRQn = 18,			//0x00040000,		//bit 18
	AD1_IRQn = 21,			//0x00200000,		//bit 21
		
	BOD_IRQn = 20,			//0x00100000,		//bit 20
	
	USB_IRQn = 22,			//0x00400000,		//bit 22
	
}IRQn_Type;

typedef enum
{
	All_IRQs,
	Enable_and_Normal_IRQs,
	Enable_and_Fast_IRQs,
}IRQn_Status_Type;

/**
  * @brief  Enables an specific the IRQ
  * @param  IRQn: Type of IRQ
	* @param  priority: Priority needed for this IRQ
	* @param  function_addr: address of the function callback
	* @param  set_as_FIRQ: true if Fast IRQ is needed
  * @retval None
  */
void VIC_EnableIRQ(IRQn_Type IRQn, uint32_t priority, uint32_t* function_addr, bool set_as_FIRQ);
/**
  * @brief  Disables an specific the IRQ
  * @param  IRQn: Type of IRQ
	* @retval None
  */
void VIC_DisableIRQ(IRQn_Type IRQn);
/**
  * @brief  Returns which iterrupts are pending
	* @retval Raw Interrupt status (VICRawIntr)
  */
uint32_t VIC_GetPendingIRQ (IRQn_Status_Type status_type);
/**
  * @brief  Returns if the specified iterrupt is activated
	* @param  IRQn: Type of IRQ
	* @retval true if activated
  */
bool VIC_GetActive(IRQn_Type IRQn);
/**
  * @brief  Returns the priority of the specified interrupt
	* @param  IRQn: Type of IRQ
	* @retval priority number
  */
uint32_t VIC_GetPriority(IRQn_Type IRQn);


#endif //_LPC2148_HAL_VIC_H
