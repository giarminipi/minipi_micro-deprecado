/**
  ******************************************************************************
  * @file    	lpc2148_hal_gpio.c
  * @author		Alejandro Gastón Alvarez
  * @version	1.2
  * @date			12/03/2016
  * @brief		Hardware Abstraction Layer for LPC2148 GPIO control
  ******************************************************************************
  * @attention
  *
  ******************************************************************************
	*/

#include <lpc2148_hal.h>

#ifdef HAL_GPIO_MODULE_ENABLED
#include <assert.h>

#define IS_P0_GPIO_PIN(pin) {pin}

// ===============================================================================
//                         ##### PORT MODE SELECTION #####
// ===============================================================================

void P0ModeSelec (uint32_t pins, P0Modes mode, uint8_t gpio_rw)
{
	uint32_t conf_h, conf_l;
	switch(mode)
	{
		case P0_MODE_UART0:
			// Checkeo que los pins seleccionados tengan esa funcionalidad
			assert( !(pins & ~P0_MODE_UART0_PINS) );
			conf_h = P0_MODE_UART0_CONF_H;
			conf_l = P0_MODE_UART0_CONF_L;
		break;

		case P0_MODE_UART1:
			// Checkeo que los pins seleccionados tengan esa funcionalidad
			assert( !(pins & ~P0_MODE_UART1_PINS) );
			conf_h = P0_MODE_UART1_CONF_H;
			conf_l = P0_MODE_UART1_CONF_L;
		break;

		case P0_MODE_PWM:
			// Checkeo que los pins seleccionados tengan esa funcionalidad
			assert( !(pins & ~P0_MODE_PWM_PINS) );
			conf_h = P0_MODE_PWM_CONF_H;
			conf_l = P0_MODE_PWM_CONF_L;
		break;

		case P0_MODE_EINT:
			// Checkeo que los pins seleccionados tengan esa funcionalidad
			assert( !(pins & ~P0_MODE_EINT_PINS) );
			conf_h = P0_MODE_EINT_CONF_H;
			conf_l = P0_MODE_EINT_CONF_L;
		break;

		case P0_MODE_I2C0:
			// Checkeo que los pins seleccionados tengan esa funcionalidad
			assert( !(pins & ~P0_MODE_I2C0_PINS) );
			conf_h = P0_MODE_I2C0_CONF_H;
			conf_l = P0_MODE_I2C0_CONF_L;
		break;

		case P0_MODE_I2C1:
			// Checkeo que los pins seleccionados tengan esa funcionalidad
			assert( !(pins & ~P0_MODE_I2C1_PINS) );
			conf_h = P0_MODE_I2C1_CONF_H;
			conf_l = P0_MODE_I2C1_CONF_L;
		break;

		case P0_MODE_MATCH:
			// Checkeo que los pins seleccionados tengan esa funcionalidad
			assert( !(pins & ~P0_MODE_MATCH_PINS) );
			conf_h = P0_MODE_MAT_CONF_H;
			conf_l = P0_MODE_MAT_CONF_L;
		break;

		case P0_MODE_CAPTURE:
			// Checkeo que los pins seleccionados tengan esa funcionalidad
			assert( !(pins & ~P0_MODE_CAPTURE_PINS) );
			conf_h = P0_MODE_CAP_CONF_H;
			conf_l = P0_MODE_CAP_CONF_L;
		break;

		case P0_MODE_SPI0:
			// Checkeo que los pins seleccionados tengan esa funcionalidad
			assert( !(pins & ~P0_MODE_SPI0_PINS) );
			conf_h = P0_MODE_SPI0_CONF_H;
			conf_l = P0_MODE_SPI0_CONF_L;
		break;

		case P0_MODE_ADC:
			// Checkeo que los pins seleccionados tengan esa funcionalidad
			assert( !(pins & ~P0_MODE_ADC_PINS) );
			conf_h = P0_MODE_ADC_CONF_H;
			conf_l = P0_MODE_ADC_CONF_L;
		break;

		case P0_MODE_SSP:
			// Checkeo que los pins seleccionados tengan esa funcionalidad
			assert( !(pins & ~P0_MODE_SSP_PINS ) );
			conf_h = P0_MODE_SSP_CONF_H;
			conf_l = P0_MODE_SSP_CONF_L;
		break;

		case P0_MODE_GPIO:
			conf_h = 0;
			conf_l = 0;
		break;

		default:
			assert(0);
		break;
	}

	// Seteo la m�scara
	uint32_t mask_h = 0;
	uint32_t mask_l = 0;
	for(uint8_t shift = 0; shift < 32; shift++)
	{
		if ( (pins >> shift) & 0x01 )
		{
			// Si count es mayor a 16 ==> tengo que utilizar el otro registro 16 pines m�s altos
			if (shift > 16)
				mask_h |= 0x3 << (shift-16)*2;			// M�scara de selecci�n de PINSEL 0b11 = 0x3
			else
				mask_l |= 0x3 << shift*2;						// M�scara de selecci�n de PINSEL 0b11 = 0x3
		}
	}

	// Selecciono los pines seg�n la m�scara
	PINSEL0 = (PINSEL0 & ~mask_l) | (conf_l & mask_l);
	PINSEL1 = (PINSEL1 & ~mask_h) | (conf_h & mask_h);

	if(mode == P0_MODE_GPIO)
	{
		if(gpio_rw)
			//Configure as output
			IODIR0 |= pins;
		else
			//Configure as input
			IODIR0 &= ~pins;
	}
}



void P1ModeSelec (uint32_t pins, P1Modes mode, uint8_t gpio_rw)
{
	assert( (pins & ~P1_All) == 0);

	switch(mode)
	{
		case P1_GPIO:
			PINSEL2 &= 0xFFFFFFF3;
			break;
		case P1_GPIO_DEBUG:
			PINSEL2 = (PINSEL2 & 0xFFFFFFF3) | 0x00000004;
			break;
		case P1_GPIO_TRACE:
			PINSEL2 = (PINSEL2 & 0xFFFFFFF3) | 0x00000008;
			break;
		case P1_DEBUG_TRACE:
			PINSEL2 |= 0x0000000C;
			break;
		default:
			assert(0);
			break;
	}

	if(mode != P1_DEBUG_TRACE)
	{
		if(gpio_rw)
			IODIR1 |= pins;
		else
			IODIR1 &= ~pins;
	}
}


// ===============================================================================
//                         ##### GPIO Read and Write #####
// ===============================================================================

uint8_t Pin_Read (Ports port, uint32_t pin)
{
  uint8_t bitstatus = 0;

	if (Port_Read (port) & pin)
		bitstatus = 1;

	return bitstatus;
}

uint32_t Port_Read (Ports port)
{
  uint32_t port_status = 0;

	switch(port)
	{
		case PORT_0:
			port_status = IOPIN0;
			break;

		case PORT_1:
			port_status = IOPIN1;
			break;
	}
  return port_status;
}


void Pin_Set (Ports port, uint32_t pin)
{
	switch(port)
	{
		case PORT_0:
			/* Check the parameters */
			assert(IS_P0_PIN(pin));

			IOSET0 |= pin;

			break;
		case PORT_1:
			/* Check the parameters */
			assert(IS_P1_PIN(pin));

			IOSET1 |= pin;

			break;
	}
}

void Port_Set (Ports port)
{
	switch(port)
	{
		case PORT_0:
			IOSET0 = 0xFFFFFFFF;
			break;

		case PORT_1:
			IOSET1 = 0xFFFFFFFF;
			break;
	}
}
void Pin_Clear (Ports port, uint32_t pin)
{
	switch(port)
	{
		case PORT_0:
			/* Check the parameters */
			assert(IS_P0_PIN(pin));

			IOCLR0 |= pin;

			break;
		case PORT_1:
			/* Check the parameters */
			assert(IS_P1_PIN(pin));

			IOCLR1 |= pin;

			break;
	}
}

void Port_Clear (Ports port)
{
	switch(port)
	{
		case PORT_0:
			IOCLR0 = 0xFFFFFFFF;
			break;

		case PORT_1:
			IOCLR1 = 0xFFFF0000;
			break;
	}
}
void Pin_Toggle (Ports port, uint32_t pin)
{
	uint32_t temp;
	switch(port)
	{
		case PORT_0:
			/* Check the parameters */
			assert(IS_P0_PIN(pin));

			temp = IOPIN0 & pin;
			if(!temp)
				IOSET0 |=  pin;
			else
				IOCLR0 |=  pin;
			break;

		case PORT_1:
			/* Check the parameters */
			assert(IS_P1_PIN(pin));

			temp = IOPIN1 & pin;
			if(!temp)
				IOSET1 |=  pin;
			else
				IOCLR1 |=  pin;
			break;
	}
}


void Port_Toggle (Ports port)
{
	uint32_t temp;

	switch(port)
	{
		case PORT_0:
			temp = IOPIN0 ^ 0xFFFFFFFF;
			IOSET0 |=  ~temp;
			IOCLR0 &=  ~temp;
			break;

		case PORT_1:
			temp = IOPIN1 ^ 0xFFFF0000;
			IOSET1 |=  ~temp;
			IOCLR1 &=  ~temp;
			break;
	}
}

// ===============================================================================
//                         ##### GPIO External Interrupt #####
// ===============================================================================
void HAL_GPIO_SetEXTI (EXTI_Type exti_type, uint32_t priority)
{
	switch(exti_type)
	{
		case EXT0:
			VIC_EnableIRQ(EINT0_IRQn, priority, (uint32_t*) &HAL_GPIO_EXTI0_Callback, false);
			break;
		case EXT1:
			VIC_EnableIRQ(EINT1_IRQn, priority, (uint32_t*) &HAL_GPIO_EXTI1_Callback, false);
			break;
		case EXT2:
			VIC_EnableIRQ(EINT2_IRQn, priority, (uint32_t*) &HAL_GPIO_EXTI2_Callback, false);
			break;
		case EXT3:
			VIC_EnableIRQ(EINT3_IRQn, priority, (uint32_t*) &HAL_GPIO_EXTI3_Callback, false);
			break;
		default:
			break;
	}
}
void HAL_GPIO_DisableEXTI (EXTI_Type exti_type)
{
	switch(exti_type)
	{
		case EXT0:
			VIC_DisableIRQ(EINT0_IRQn);
			break;
		case EXT1:
			VIC_DisableIRQ(EINT1_IRQn);
			break;
		case EXT2:
			VIC_DisableIRQ(EINT2_IRQn);
			break;
		case EXT3:
			VIC_DisableIRQ(EINT3_IRQn);
			break;
		default:
			break;
	}
}
/**
  * @brief  EXTI line detection callbacks.
  * @param  GPIO_Pin: Specifies the pins connected EXTI line
  * @retval None
  */
__weak void HAL_GPIO_EXTI0_Callback(void)
{
  /* NOTE: This function Should not be modified, when the callback is needed,
           the HAL_GPIO_EXTI_Callback could be implemented in the user file
   */
}
/**
  * @brief  EXTI line detection callbacks.
  * @param  GPIO_Pin: Specifies the pins connected EXTI line
  * @retval None
  */
__weak void HAL_GPIO_EXTI1_Callback(void)
{
  /* NOTE: This function Should not be modified, when the callback is needed,
           the HAL_GPIO_EXTI_Callback could be implemented in the user file
   */
}
/**
  * @brief  EXTI line detection callbacks.
  * @param  GPIO_Pin: Specifies the pins connected EXTI line
  * @retval None
  */
__weak void HAL_GPIO_EXTI2_Callback(void)
{
  /* NOTE: This function Should not be modified, when the callback is needed,
           the HAL_GPIO_EXTI_Callback could be implemented in the user file
   */
}
/**
  * @brief  EXTI line detection callbacks.
  * @param  GPIO_Pin: Specifies the pins connected EXTI line
  * @retval None
  */
__weak void HAL_GPIO_EXTI3_Callback(void)
{
  /* NOTE: This function Should not be modified, when the callback is needed,
           the HAL_GPIO_EXTI_Callback could be implemented in the user file
   */
}
#endif	// HAL_GPIO_MODULE_ENABLED
