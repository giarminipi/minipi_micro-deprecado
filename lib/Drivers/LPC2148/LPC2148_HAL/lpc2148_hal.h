/**
  ******************************************************************************
  * @file    lpc2148_hal.c
  * @author  
  * @version 
  * @date    
  * @brief   
  ******************************************************************************
  * @attention
  *
  ******************************************************************************
	*/

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef _LPC2148_HAL_H
#define _LPC2148_HAL_H

/* Inludes ------------------------------------------------------------------*/
#include <lpc2148_hal_conf.h>
#include <lpc2148_hal_vic.h>
#include <lpc2148_hal_pll.h>

#ifdef HAL_GPIO_MODULE_ENABLED
	#include <lpc2148_hal_gpio.h>
#endif

#ifdef HAL_TIM_MODULE_ENABLED
	#include <lpc2148_hal_tim.h>
#endif

#ifdef HAL_SSP_MODULE_ENABLED
	#include <lpc2148_hal_ssp.h>
#endif

#ifdef HAL_UART_MODULE_ENABLED
	#include <lpc2148_hal_uart.h>
#endif

#ifdef HAL_PWM_MODULE_ENABLED
	#include <lpc2148_hal_pwm.h>
#endif

#ifdef HAL_SPI_MODULE_ENABLED
	#include <lpc2148_hal_spi.h>
#endif


typedef enum
{
	DIV_2 = 2,
	DIV_4 = 0,
} APB_Mode;

void setAPBDivider (APB_Mode mode);
uint32_t perClk (void);


#endif // End _LPC2148_HAL_H


