/**
  ******************************************************************************
  * @file    	lpc2148_hal_gpio.h
  * @author	Alejandro Gastón Alvarez
  * @version	1.2
  * @date	12/03/2016
  * @brief	Hardware Abstraction Layer for LPC2148 GPIO control
  ******************************************************************************
  * @attention
  *
  ******************************************************************************
	*/

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef _LPC2148_HAL_GPIO_H
#define _LPC2148_HAL_GPIO_H

/* Includes ------------------------------------------------------------------*/
#include <lpc2148_hal_def.h>


typedef enum
{
  PORT_0 = 0,
	PORT_1,// = 1
} Ports;

typedef enum
{
	P0_MODE_GPIO=0,		/*!< General Input Mode                   */
	P0_MODE_UART0,		/*!< Output Push Pull Mode                 */
	P0_MODE_UART1,		/*!< Output Push Pull Mode                 */
	P0_MODE_PWM,			/*!< Output Open Drain Mode                */
	P0_MODE_EINT,     /*!< Alternate Function Push Pull Mode     */
	P0_MODE_I2C0,     /*!< Alternate Function Open Drain Mode    */
	P0_MODE_I2C1,     /*!< Alternate Function Open Drain Mode    */
	P0_MODE_MATCH,    /*!< Alternate Function Open Drain Mode    */
	P0_MODE_CAPTURE,	/*!< Alternate Function Open Drain Mode    */
	P0_MODE_SPI0,     /*!< External Interrupt Mode with Rising edge trigger detection          */
	P0_MODE_SSP,     	/*!< External Interrupt Mode with Rising edge trigger detection          */
	P0_MODE_ADC,      /*!< External Interrupt Mode with Rising edge trigger detection          */
}P0Modes;

typedef enum
{
	P1_GPIO  = 0,
	P1_GPIO_DEBUG = 1,
	P1_GPIO_TRACE = 2,
	P1_DEBUG_TRACE = 3,
} P1Modes;

typedef enum
{
	PIN_CLEAR = 0,
	PIN_SET = 1,
}PIN_State;

typedef enum
{
  EXT0 = 0,
	EXT1 = 1,
	EXT2 = 2,
	EXT3 = 3,
} EXTI_Type;

#define PIN_0                 ((uint32_t)0x00000001)  /* Pin 0 selected    */
#define PIN_1                 ((uint32_t)0x00000002)  /* Pin 1 selected    */
#define PIN_2                 ((uint32_t)0x00000004)  /* Pin 2 selected    */
#define PIN_3                 ((uint32_t)0x00000008)  /* Pin 3 selected    */
#define PIN_4                 ((uint32_t)0x00000010)  /* Pin 4 selected    */
#define PIN_5                 ((uint32_t)0x00000020)  /* Pin 5 selected    */
#define PIN_6                 ((uint32_t)0x00000040)  /* Pin 6 selected    */
#define PIN_7                 ((uint32_t)0x00000080)  /* Pin 7 selected    */
#define PIN_8                 ((uint32_t)0x00000100)  /* Pin 8 selected    */
#define PIN_9                 ((uint32_t)0x00000200)  /* Pin 9 selected    */
#define PIN_10                ((uint32_t)0x00000400)  /* Pin 10 selected   */
#define PIN_11                ((uint32_t)0x00000800)  /* Pin 11 selected   */
#define PIN_12                ((uint32_t)0x00001000)  /* Pin 12 selected   */
#define PIN_13                ((uint32_t)0x00002000)  /* Pin 13 selected   */
#define PIN_14                ((uint32_t)0x00004000)  /* Pin 14 selected   */
#define PIN_15                ((uint32_t)0x00008000)  /* Pin 15 selected   */
#define PIN_16                ((uint32_t)0x00010000)  /* Pin 16 selected   */
#define PIN_17                ((uint32_t)0x00020000)  /* Pin 17 selected   */
#define PIN_18                ((uint32_t)0x00040000)  /* Pin 18 selected   */
#define PIN_19                ((uint32_t)0x00080000)  /* Pin 19 selected   */
#define PIN_20                ((uint32_t)0x00100000)  /* Pin 20 selected   */
#define PIN_21                ((uint32_t)0x00200000)  /* Pin 21 selected   */
#define PIN_22                ((uint32_t)0x00400000)  /* Pin 22 selected   */
#define PIN_23                ((uint32_t)0x00800000)  /* Pin 23 selected   */
#define PIN_24                ((uint32_t)0x01000000)  /* Pin 24 selected   */
#define PIN_25                ((uint32_t)0x02000000)  /* Pin 25 selected   */
#define PIN_26                ((uint32_t)0x04000000)  /* Pin 26 selected   */
#define PIN_27                ((uint32_t)0x08000000)  /* Pin 27 selected   */
#define PIN_28                ((uint32_t)0x10000000)  /* Pin 28 selected   */
#define PIN_29                ((uint32_t)0x20000000)  /* Pin 29 selected   */
#define PIN_30                ((uint32_t)0x40000000)  /* Pin 10 selected   */
#define PIN_31                ((uint32_t)0x80000000)  /* Pin 31 selected   */
#define P0_All               	((uint32_t)0xFFFFFFFF)  /* All pins selected */
#define P1_All               	((uint32_t)0xFFFF0000)  /* All pins selected */
#define NO_PIN               	((uint32_t)0x00000000)  /* All pins selected */

#define IS_P0_PIN(PIN)				(((PIN) == PIN_0) || ((PIN) == PIN_1) || \
															 ((PIN) == PIN_2) || ((PIN) == PIN_3) || \
                               ((PIN) == PIN_4) || ((PIN) == PIN_5) || \
                               ((PIN) == PIN_6) || ((PIN) == PIN_7) || \
                               ((PIN) == PIN_8) || ((PIN) == PIN_9) || \
                               ((PIN) == PIN_10) || ((PIN) == PIN_11) || \
                               ((PIN) == PIN_12) || ((PIN) == PIN_13) || \
															 ((PIN) == PIN_14) || ((PIN) == PIN_15) || \
															 ((PIN) == PIN_16) || ((PIN) == PIN_17) || \
															 ((PIN) == PIN_18) || ((PIN) == PIN_19) || \
															 ((PIN) == PIN_20) || ((PIN) == PIN_21) || \
															 ((PIN) == PIN_22) || ((PIN) == PIN_23) || \
															 ((PIN) == PIN_24) || ((PIN) == PIN_25) || \
															 ((PIN) == PIN_26) || ((PIN) == PIN_27) || \
															 ((PIN) == PIN_28) || ((PIN) == PIN_29) || \
															 ((PIN) == PIN_30) || ((PIN) == PIN_31))

#define IS_P1_PIN(PIN)				(((PIN) == PIN_16) || ((PIN) == PIN_17) || \
															 ((PIN) == PIN_18) || ((PIN) == PIN_19) || \
															 ((PIN) == PIN_20) || ((PIN) == PIN_21) || \
															 ((PIN) == PIN_22) || ((PIN) == PIN_23) || \
															 ((PIN) == PIN_24) || ((PIN) == PIN_25) || \
															 ((PIN) == PIN_26) || ((PIN) == PIN_27) || \
															 ((PIN) == PIN_28) || ((PIN) == PIN_29) || \
															 ((PIN) == PIN_30) || ((PIN) == PIN_31))

#define  P0_MODE_PWM_PINS     ((uint32_t)0x00200383)			// 00000000001000000000001110000011
#define  P0_MODE_PWM_CONF_H		((uint32_t)0x00000400)			// 00000000000000000000010000000000
#define  P0_MODE_PWM_CONF_L		((uint32_t)0x000A800A)			// 00000000000010101000000000001010

#define  P0_MODE_CAPTURE_PINS	((uint32_t)0x706E0C54) 			// 01110000011011100000110001010100
#define  P0_MODE_CAP_CONF_H		((uint32_t)0x3A002CD4)			// 00111010000000000010110011010100
#define  P0_MODE_CAP_CONF_L		((uint32_t)0x00A02220)			// 00000000101000000010001000100000

#define  P0_MODE_MATCH_PINS		((uint32_t)0x305E302C)			// 00110000010111100011000000101100
#define  P0_MODE_MAT_CONF_H		((uint32_t)0x0F00313C)			// 00001111000000000011000100111100
#define  P0_MODE_MAT_CONF_L		((uint32_t)0x0A0008B0)			// 00001010000000000000100010110000

#define  P0_MODE_ADC_PINS     ((uint32_t)0X7260B570)			// 01110010011000001011010101110000
#define  P0_MODE_ADC_CONF_H		((uint32_t)0x15041800)			// 00010101000001000001100000000000
#define  P0_MODE_ADC_CONF_L		((uint32_t)0xCF333F00)			// 11001111001100110011111100000000

#define  P0_MODE_EINT_PINS    ((uint32_t)0X4011C28A)			// 01000000000100011100001010001010
#define  P0_MODE_EINT_CONF_H	((uint32_t)0x20000301)			// 00100000000000000000001100000001
#define  P0_MODE_EINT_CONF_L	((uint32_t)0xA00CC0CC)			// 10100000000011001100000011001100

#define  P0_MODE_UART0_PINS		((uint32_t)0x00000003)			// 00000000000000000000000000000011
#define  P0_MODE_UART0_CONF_H	((uint32_t)0x00000000)			// 00000000000000000000000000000000
#define  P0_MODE_UART0_CONF_L	((uint32_t)0x00000005)			// 00000000000000000000000000000101

#define  P0_MODE_UART1_PINS		((uint32_t)0x0000FF00)			// 00000000000000001111111100000000
#define  P0_MODE_UART1_CONF_H	((uint32_t)0x00000000)			// 00000000000000000000000000000000
#define  P0_MODE_UART1_CONF_L	((uint32_t)0x55550000)			// 01010101010101010000000000000000

#define  P0_MODE_I2C0_PINS    ((uint32_t)0x0000000C)			// 00000000000000000000000000001100
#define  P0_MODE_I2C0_CONF_H	((uint32_t)0x00000000)			// 00000000000000000000000000000000
#define  P0_MODE_I2C0_CONF_L	((uint32_t)0x00000050)			// 00000000000000000000000001010000

#define  P0_MODE_I2C1_PINS    ((uint32_t)0x00004800)			// 00000000000000000100100000000000
#define  P0_MODE_I2C1_CONF_H	((uint32_t)0x00000000)			// 00000000000000000000000000000000
#define  P0_MODE_I2C1_CONF_L	((uint32_t)0x30C00000)			// 00110000110000000000000000000000

#define  P0_MODE_SPI0_PINS    ((uint32_t)0x000000F0)			// 00000000000000000000000011110000
#define  P0_MODE_SPI0_CONF_H	((uint32_t)0x00000000)			// 00000000000000000000000000000000
#define  P0_MODE_SPI0_CONF_L	((uint32_t)0x00005500)			// 00000000000000000101010100000000

#define  P0_MODE_SSP_PINS     ((uint32_t)0X001E0000)			// 00000000000111100000000000000000
#define  P0_MODE_SSP_CONF_H		((uint32_t)0x000002A8)			// 00000000000000000000001010101000
#define  P0_MODE_SSP_CONF_L		((uint32_t)0x00000000)			// 00000000000000000000000000000000



/**
  * @brief Port 0 Pin mode seletion
  * @param[in]  pins Pins to be set to the specific mode (can be added eg. PIN_3 | PIN_8)
  * @param[in]  mode Function mode selected for the pins
  * @param[in]  gpio_rw If GPIO mode is selected, put "1" for output and "0" for input
  * @retval
  */
void P0ModeSelec (uint32_t pins, P0Modes mode, uint8_t gpio_rw);
/**
  * @brief Port 1 Pin mode seletion
  * @param[in]  pins Pins to be set to the specific mode (can be added eg. PIN_3 | PIN_8)
  * @param[in]  mode Function mode selected for the pins
  * @param[in]  gpio_rw If GPIO mode is selected, put "1" for output and "0" for input
  * @retval
  */
void P1ModeSelec (uint32_t pins, P1Modes mode, uint8_t gpio_rw);
/**
  * @brief Reads an specific pin
  * @param[in]  port Port witch the pin belongs to
  * @param[in]  pin Pin number
  * @retval returns pin state
  */
uint8_t Pin_Read (Ports port, uint32_t pin);
/**
  * @brief Reads an specific port
  * @param[in]  port Port to be read
  * @retval returns port state
  */
 uint32_t Port_Read (Ports port);
 /**
   * @brief Sets an specicif pin
   * @param[in]  port Port witch the pin belongs to
   * @param[in]  pin Pin number
   */
void Pin_Set (Ports port, uint32_t pin);
/**
  * @brief Writes ones to an specicif port
  * @param[in]  port Port to be written
  */
void Port_Set (Ports port);
/**
  * @brief Clear an specicif pin
  * @param[in]  port Port witch the pin belongs to
  * @param[in]  pin Pin number
  */
void Pin_Clear (Ports port, uint32_t pin);
/**
  * @brief Clears an specicif port
  * @param[in]  port Port to be cleared
  */
void Port_Clear (Ports port);
/**
  * @brief Toggles an specicif pin
  * @param[in]  port Port witch the pin belongs to
  * @param[in]  pin Pin number
  */
void Pin_Toggle (Ports port, uint32_t pin);
/**
  * @brief Toggle an specicif port
  * @param[in]  port Port to be toggled
  */
void Port_Toggle (Ports port);

//void P0_Init (GPIO_InitTypeDef pins, P0_MODEs mode);


//------------------------------------------------------------------------------------------
//																EXTERNAL INTERRUP MANAGMENT
//------------------------------------------------------------------------------------------
typedef enum
{
	EXTI_LINE_0,
	EXTI_LINE_1,
	EXTI_LINE_2,
	EXTI_LINE_3,
}EXTI_LINE;

typedef enum
{
	EXTI_LEVEL = 0,
	EXTI_EDGE = 1,
}EXTI_MODE;

typedef enum
{
	EXTI_LOW_ACTIVE = 0,
	EXTI_FALLING_EDGE = 0,
	EXTI_HIGH_ACTIVE = 1,
	EXTI_RISING_EDGE = 1,
}EXTI_POLAR;


void HAL_GPIO_EXTI0_Callback(void);
void HAL_GPIO_EXTI1_Callback(void);
void HAL_GPIO_EXTI2_Callback(void);
void HAL_GPIO_EXTI3_Callback(void);

#endif	// End _LPC2148_HAL_GPIO_H
