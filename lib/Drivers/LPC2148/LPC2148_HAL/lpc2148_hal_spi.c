/**
  ******************************************************************************
  * @file    lpc2148_hal_spi.c
  * @author  Pablo D.Folino y Sergio Alberino
  * @version 1.0
  * @date    12/05/2015
  * @brief   Funciones SPI
  ******************************************************************************
  * @attention
  *
  ******************************************************************************
	*/
	
#include <lpc2148_hal.h>

const char *SPI_status[5] = {
	"Abort",
	"Mode Fault",
	"Read Overrun",
	"Write Collision",
	"Transfer Complete"
};

//TODO Sacar de aca
#define pfreq 30000000

int initSPI(char prescaler, SPI_Modes mode, SPI_CLK_Pol clkpol, SPI_PHASE phase, SPI_Tx_ORDER bitorder, SPI_IRQ irq, SPI_TxBits num_bits)
{
	int spi_freq = 0;
	
	// Configure SPI clock
	if (prescaler % 2 == 0)
	{
		S0SPCCR = prescaler;
		spi_freq = pfreq / prescaler;
		
		// Configure bit enable
		S0SPCR = 0x4;
		
		// Configure clock phase
		S0SPCR = S0SPCR | phase<<3;
		
		// Configure clock mode
		S0SPCR = S0SPCR | clkpol<<4;
		
		// Configure operation mode
		S0SPCR = S0SPCR | mode<<5;
		
		// Configure bit order mode
		S0SPCR = S0SPCR | bitorder<<6;
		
		// Configure SPI interrupt
		S0SPCR = S0SPCR | irq<<7;
		
		// Configure operation mode
		S0SPCR = S0SPCR | num_bits<<8;
	}
	else
	{
		spi_freq = 0;
	}
	
	// Return no errors
	return spi_freq;
}

SPI_Status deinitSPI(void)
{
	S0SPCR = 0;
	
	// Return no errors
	return statusSPI();
}

SPI_Status statusSPI (void)
{
	SPI_Status status;
	
	// Read the status of the SPI (0xF8 because of the reserved bits)
	status = (SPI_Status) (S0SPSR & 0xF8);
	
	return status;
}

SPI_Status writeSPI (int data)
{
	S0SPDR = data;
	
	return statusSPI();
}

SPI_Status readSPI (int *data)
{
	*data = S0SPDR;
	
	return statusSPI();
}
