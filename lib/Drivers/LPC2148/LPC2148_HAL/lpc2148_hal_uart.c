/**
  ******************************************************************************
  * @file    lpc2148_hal_uart.c
  * @author
  * @version
  * @date
  * @brief
  ******************************************************************************
  * @attention
  *
  ******************************************************************************
	*/

#include <lpc2148_hal.h>

//#include <float.h>
#include <math.h>
#include <limits.h>
#include <stdlib.h>
#include <stdbool.h>

/* Private Functions ---------------------------------------------------------- */
/**
 * @brief Obtiene los divisores y multiplicadores necesarios para el seteo del baudrate
 * @details Calcula los divisores multiplicadores y divisores necesarios para setear el baudrate con el menor error posible
 * 
 * @param clock	
 * @param baudrate
 * @param best_dlab
 * @param best_mulval
 * @param best_divval 
 * @return Error Status
 */
uint32_t uart_divisors_algorithm ( uint32_t clock, uint32_t baudrate, uint16_t* best_dlab, uint8_t* best_mulval, uint8_t* best_divval )
{
    // Divido el clock por 16
    clock = clock >> 4; // clock <- (clock/16)

    /* En el Uart IP block, baud rate se calcula usando los registros FDR y DLL-DLM*/
    //Formula original:
    //      BaudRate    =                           Pclk             *              mulval
    //                                  ---------------------------     -------------------------------
    //                                    16 * (256 * DLM + DLL)                (mulval +   divval)
    // El valor de mulval y divval debe cumplir con las siguientes expresiones:
    //                  0 < mulval <= 15          0 <= divval <= 15

    //Algoritmo para llegar al Baudrate con el menor error

    bool flag = true;
    uint32_t min_error = UINT_MAX;
    *best_divval = 0;
    *best_mulval = 0;
    *best_dlab = 1;
    
    // Lo que esta comentado es para hacerlo supuestamente más preciso
    // pero como trabaja con floats tarda más y da exactamente lo mismo

    //  float min_error = FLT_MAX;
    //  float fit = 1.0 * pclk / 16;

    // Fitting algorithm
    for ( int divval = 15; divval >= 0 && flag; divval-- )
    {
        for ( int mulval = 15; mulval > 0 && flag; mulval-- )
        {
            uint32_t frac = clock * mulval / ( mulval + divval );
            uint16_t dlab = round ( 1.0 * frac / baudrate );
            uint32_t error = abs ( baudrate * dlab - frac );

            //          float frac = fit * mulval / (mulval + divval);
            //          int dlab = round ( frac / baudrate );
            //          float error = abs ( baudrate - frac / dlab );

            if ( error < min_error )
            {
                min_error = error;
                *best_mulval = mulval;
                *best_divval = divval;
                *best_dlab = dlab;
            }

            if ( min_error == 0 )
                flag = false;
        }
    }

    return 0;
}
/* End of Private Functions ---------------------------------------------------- */



/* Public Functions ----------------------------------------------------------- */
/**
 * @brief Genera la estructura de configuración de la UART 
 * @details Crea la estructura de configuración de la UART basada en los parámetros obtenidos
 * 
 * @param UART_InitStruct	Puntero a una estructura de configuración
 * @param baudrate				Baudrate de la comunicación
 * @param databits				Wordlength #UART_DATABIT_Type
 * @param Parity					
 * @param Stopbits 
 * @param fifo_type
 * @param int_type
 *
 * @return Error Status
 */
void UART_ConfigStructInit ( UART_Config* UART_InitStruct, uint32_t baudrate, UART_DATABIT_Type databits, UART_PARITY_Type Parity, UART_STOPBIT_Type Stopbits, UART_FIFO_Type fifo_type, UART_INT_Type int_type )
{
	UART_InitStruct->baud_rate = baudrate;
	UART_InitStruct->databits = databits;
	UART_InitStruct->parity = Parity;
	UART_InitStruct->stopbits = Stopbits;
	UART_InitStruct->FIFO_type = fifo_type;
	UART_InitStruct->INT_type = int_type;
}


// ===============================================================================
//                         					(DE)INIT FUNCTIONS
// ===============================================================================

void UART_Init ( UARTx t_uart, UART_Config *UART_Conf )
{
	LPC_UART_Regs *UARTx;

	// Elijo la UART
	if (t_uart == UART0)
		UARTx = (LPC_UART_Regs *) LPC_UART0_BASE;
	else
		UARTx = (LPC_UART_Regs *) LPC_UART1_BASE;

	// TODO: Leer la configuración del Clock
	uint32_t clock = 30000000;

	//******************* Reset UART to default state *******************
	UARTx->IER = 0;			// Disable interrupt
	UARTx->FCR = 0; 		// Disable FIFO        //TODO solo escribir el bit 0 en 0
	UARTx->TER = 0;			// Disable Tx
	UARTx->LCR = 0;			// Set LCR to default state
	UARTx->ACR = 0;			// Set ACR to default state

	if (t_uart == UART1)
		UARTx->MCR = 0; 		// Set Modem Control to default state


	//******************* Configure UART *******************
	uint8_t tmp = 0;
	tmp |= UART_Conf->databits;		// Databits
	tmp |= UART_Conf->stopbits;		// Stop Bit
	tmp |= UART_Conf->parity;		// Parity
	UARTx->LCR = tmp & UART_LCR_BITMASK;

	// Set Divisors
	uint16_t dlab;
	uint8_t mulval, divval;
	uart_divisors_algorithm ( clock, UART_Conf->baud_rate, &dlab, &mulval, &divval );
	UARTx->LCR |= UART_LCR_DLAB_EN;
	UARTx->DLM = UART_LOAD_DLM ( dlab );
	UARTx->DLL = UART_LOAD_DLL ( dlab );
	UARTx->FDR = ( UART_FDR_MULVAL ( mulval ) | UART_FDR_DIVADDVAL ( divval ) ) & UART_FDR_BITMASK;
	UARTx->LCR &= ( ~UART_LCR_DLAB_EN ) & UART_LCR_BITMASK;

	// Reseteo Tx & RX y configuro la FIFO
	UARTx->FCR = UART_FCR_RX_RS | UART_FCR_TX_RS;
	UARTx->FCR = UART_Conf->FIFO_type;

	// Seteo la configuración de la Interrupción
	UARTx->IER = UART_Conf->INT_type;
	
	//Habilito la transmisión de datos
	UARTx->TER = UART_TER_TXEN;
}


void UART_DeInit ( UARTx t_uart )
{
	LPC_UART_Regs *UARTx;

	// Elijo la UART
	if (t_uart == UART0)
		UARTx = (LPC_UART_Regs *) LPC_UART0_BASE;
	else
		UARTx = (LPC_UART_Regs *) LPC_UART1_BASE;
	
	//******************* Reset UART to default state *******************
	UARTx->IER = 0;			// Disable interrupt
	UARTx->FCR = 0; 		// Disable FIFO        //TODO solo escribir el bit 0 en 0
	UARTx->TER = 0;			// Disable Tx
	UARTx->LCR = 0;			// Set LCR to default state
	UARTx->ACR = 0;			// Set ACR to default state

	if (t_uart == UART1)
		UARTx->MCR = 0; 		// Set Modem Control to default state
}


// ===============================================================================
//                         					SEND FUNCTIONS
// ===============================================================================
void UART_SendByte ( UARTx t_uart, uint8_t Data )
{
    LPC_UART_Regs *UARTx;

	// Elijo la UART
    if (t_uart == UART0)
    	UARTx = (LPC_UART_Regs *) LPC_UART0_BASE;
    else
    	UARTx = (LPC_UART_Regs *) LPC_UART1_BASE;

		uint32_t timeOut;
		
		//Wait for data
		for ( timeOut = UART_BLOCKING_TIMEOUT; !( UARTx->LSR & UART_LSR_THRE ) && timeOut; timeOut-- );
		
		// Envío el dato
		if ( timeOut )
			UARTx->THR = ( Data & UART_THR_MASKBIT );
}

uint32_t UART_Send ( UARTx t_uart, uint8_t *txbuf, uint32_t buflen)
{
	LPC_UART_Regs *UARTx;

	// Elijo la UART
	if (t_uart == UART0)
		UARTx = (LPC_UART_Regs *) LPC_UART0_BASE;
	else
		UARTx = (LPC_UART_Regs *) LPC_UART1_BASE;
	
	
	uint32_t bytes_sent = 0, bytes_to_send = buflen;
	uint32_t timeOut;
	
	while( bytes_to_send )
	{
		//Wait for data
		for ( timeOut = UART_BLOCKING_TIMEOUT; !( UARTx->LSR & UART_LSR_THRE ) && timeOut; timeOut-- );
		
		// Time out!
		if ( !timeOut )
			break;
		
		//Write bytes to FIFO
		for ( uint32_t fifo_cnt = UART_TX_FIFO_SIZE; fifo_cnt && bytes_to_send; fifo_cnt--, bytes_to_send--, bytes_sent++ )
			UART_SendByte ( t_uart, *(txbuf++) );
	}

	return bytes_sent;
}


// ===============================================================================
//                         					RECEIVE FUNCTIONS
// ===============================================================================
uint8_t UART_ReceiveByte ( UARTx t_uart, uint8_t* data )
{
    LPC_UART_Regs *UARTx;

	// Elijo la UART
    if (t_uart == UART0)
    	UARTx = (LPC_UART_Regs *) LPC_UART0_BASE;
    else
    	UARTx = (LPC_UART_Regs *) LPC_UART1_BASE;

    // Obetngo el dato
    *data = ( UARTx->RBR & UART_RBR_MASKBIT );
		
		return 0;
}


uint32_t UART_Receive ( UARTx t_uart, uint8_t *rxbuf, uint32_t buflen )
{
	LPC_UART_Regs *UARTx;

	// Elijo la UART
	if (t_uart == UART0)
		UARTx = (LPC_UART_Regs *) LPC_UART0_BASE;
	else
		UARTx = (LPC_UART_Regs *) LPC_UART1_BASE;
	
	uint32_t bRecv = 0;
	uint32_t timeOut;

	for ( uint32_t bToRecv = buflen; bToRecv; bToRecv--, bRecv++ )
	{
		//Wait for data
		for ( timeOut = UART_BLOCKING_TIMEOUT ;!( UARTx->LSR & UART_LSR_RDR ) && timeOut; timeOut-- );
		
		// Time out!
		if ( !timeOut )
			break;
		
		// Get data from the buffer
		UART_ReceiveByte ( t_uart, rxbuf++ );
	}
	
	return bRecv;
}





/* UART operate functions -------------------------------------------------------*/
void UART_ForceBreak ( UARTx t_uart )
{
    LPC_UART_Regs *UARTx;

	// Elijo la UART
    if (t_uart == UART0)
    	UARTx = (LPC_UART_Regs *) LPC_UART0_BASE;
    else
    	UARTx = (LPC_UART_Regs *) LPC_UART1_BASE;

    // Fuerzo el Break
    UARTx->LCR |= UART_LCR_BREAK_EN;
}

bool UART_CheckBusy ( UARTx t_uart )
{
	LPC_UART_Regs *UARTx;

	// Elijo la UART
	if (t_uart == UART0)
		UARTx = (LPC_UART_Regs *) LPC_UART0_BASE;
  else
		UARTx = (LPC_UART_Regs *) LPC_UART1_BASE;
	
	if ( UARTx->LSR & UART_LSR_TEMT )
		return true;
	else
		return false;
}

void UART_Tx_Enable ( UARTx t_uart )
{
	LPC_UART_Regs *UARTx;

	// Elijo la UART
	if (t_uart == UART0)
		UARTx = (LPC_UART_Regs *) LPC_UART0_BASE;
	else
		UARTx = (LPC_UART_Regs *) LPC_UART1_BASE;
	
  // Habilito la Tx
	UARTx->TER |= UART_TER_TXEN;
}

void UART_Tx_Disable ( UARTx t_uart )
{
	LPC_UART_Regs *UARTx;

	// Elijo la UART
	if (t_uart == UART0)
		UARTx = (LPC_UART_Regs *) LPC_UART0_BASE;
	else
		UARTx = (LPC_UART_Regs *) LPC_UART1_BASE;
	
  // Habilito la Tx
	UARTx->TER &= ( ~UART_TER_TXEN ) & UART_TER_BITMASK;
}

void UART_IntConfig ( UARTx t_uart, UART_INT_Type int_type )
{
	LPC_UART_Regs *UARTx;

	// Elijo la UART
	if (t_uart == UART0)
		UARTx = (LPC_UART_Regs *) LPC_UART0_BASE;
	else
		UARTx = (LPC_UART_Regs *) LPC_UART1_BASE;
	
    // Reseteo Tx & RX y configuro la FIFO
	UARTx->IER = int_type;
}

void UART_FIFOConfig ( UARTx t_uart, UART_FIFO_Type fifo_type )
{
	LPC_UART_Regs *UARTx;

	// Elijo la UART
	if (t_uart == UART0)
		UARTx = (LPC_UART_Regs *) LPC_UART0_BASE;
	else
		UARTx = (LPC_UART_Regs *) LPC_UART1_BASE;
	
    // Reseteo Tx & RX y configuro la FIFO
	UARTx->FCR = UART_FCR_RX_RS | UART_FCR_TX_RS;
	UARTx->FCR = fifo_type;
}

