/**
  ******************************************************************************
  * @file    lpc2148_hal_pll.h
  * @author  Alejandro Gast�n Alvarez
  * @version 0.1
  * @date    19/04/2016
  * @brief   Hardware Abstraction Layer for LPC2148 PLL control
  ******************************************************************************
  * @attention
  *
  ******************************************************************************
	*/


/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef _LPC2148_HAL_PLL_H
#define _LPC2148_HAL_PLL_H

/* Includes ------------------------------------------------------------------*/
#include <lpc2148_hal_def.h>
#include <stdbool.h>

typedef union
{
	uint16_t reg;
	struct {
		uint8_t pll_msel : 5;
		uint8_t pll_psel : 2;
		uint8_t pll_resv : 1;
		uint8_t pll_plle : 1;
		uint8_t pll_pllc : 1;
		uint8_t pll_ploc : 1;
		uint8_t pll_resv2: 5;
	}bits;
}PLL_Status;

typedef enum
{
	PLL_OFF_DISCONET = 0,
	PLL_ON_DISCONETED = 1,
	PLL_ON_CONETED = 3,
}PLL_Modes;


static void setPLL (PLL_Modes mode);
void configurePLL (uint32_t osc_freq, uint32_t core_freq);
PLL_Status statusPLL (void);
uint32_t oscFreq (void);
uint32_t clkFreq (void);

#endif
