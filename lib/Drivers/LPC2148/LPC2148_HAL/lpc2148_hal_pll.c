/**
  ******************************************************************************
  * @file    lpc2148_hal_pll.c
  * @author  Alejandro Gast�n Alvarez
  * @version 0.1
  * @date    19/04/2016
  * @brief   Hardware Abstraction Layer for LPC2148 PLL control
  ******************************************************************************
  * @attention
  *
  ******************************************************************************
	*/


/* Includes ------------------------------------------------------------------*/
#include <lpc2148_hal.h>
#include <assert.h>

// PLL0CON Register
//#define PLLE_bit 0x1
//#define PLLC_bit 0x2

//// PLL0CFG Register
//#define MSEL_mask 0x1F
//#define PSEL_mask 0x60

static uint32_t osc = 12000000;

static void setPLL (PLL_Modes mode)
{
	PLL0CON = (uint8_t) mode;
	
	// Set PLL feed squence
	PLL0FEED = 0xAA;
	PLL0FEED = 0x55;
}

void configurePLL (uint32_t osc_freq, uint32_t core_freq)
{
	// Aborto si se elige mal la frecuencia del oscilador
	assert( !(osc_freq < 10 || osc_freq > 25) );
	assert( !(osc_freq > core_freq) );

	// Desconecto el PLL
	setPLL (PLL_OFF_DISCONET);
	
	// Guardo el valor del oscilador en Hz
	osc = osc_freq * 1000000;
	
	// Seteo el multiplicador para obtener la frecuencia de clock
	uint8_t msel = core_freq / osc_freq - 1;
//	PLL0CFG = mult | (PLL0CFG & MSEL_mask);
//	clk = osc_freq * mult;		// guardo el valor del clock
	
	// Seteo el divisor para setear la frecuencia del Current Controlled Oscillators (CCO)
	uint8_t psel = 0;
	for (; psel<4; psel++)		// Calculo el divisor
	{
		uint32_t cco = core_freq * ( 1<<(psel+1) );
		if ( (cco >= 156) && (cco <= 320) )
			break;
	}
//	PLL0CFG = div | (PLL0CFG & PSEL_mask);		//Seteo el divisor
	PLL0CFG = (psel << 5) | msel;		//Seteo el divisor
	
	// Enable PLL
	setPLL (PLL_ON_DISCONETED);
	
	// Wait for PLL Lock
	while ( (bool) statusPLL().bits.pll_ploc == false);
	
	// Conect PLL
	setPLL (PLL_ON_CONETED);
	
}

PLL_Status statusPLL (void)
{
	PLL_Status status;

	status.reg	= (uint16_t) PLL0STAT;
	
	return status;
}

uint32_t oscFreq (void)
{
	return osc;
}

uint32_t clkFreq (void)
{
	return osc * (statusPLL().bits.pll_msel+1);
}

