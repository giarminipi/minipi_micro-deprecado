/**
  ******************************************************************************
  * @file    lpc2148_hal_tim.h
  * @author  JMGG & JSTC & AB
  * @version 4.0
  * @date    08/03/2016
  * @brief   Inicializacion y configuracion de timers
	*					 Timers initilization and configuration
  ******************************************************************************
  * @attention
  *
  ******************************************************************************
	*/

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef _LPC2148_HAL_TIM_H
#define _LPC2148_HAL_TIM_H

/* Includes ------------------------------------------------------------------*/
#include <lpc2148_hal_def.h>

#define PRESCALE 		60000		/**< 60000 PCLK clock cycles to increment TC by 1 */
#define DELAY_MS 		500			/**< 0.5 Seconds Delay */
#define PRESCALE_T0 6				/**< 6 PCLK clock cycles to increment TC by 1 : 100nseg */
#define DELAY_MS_T0 1				/**< 0.001 Seconds Delay */
#define PRESCALE_T1 60000		/**< 60000 PCLK clock cycles to increment TC by 1 : 1mseg */
#define DELAY_MS_T1 500			/**< 0.5 Seconds Delay */
#define MR0I				(1<<0)	/**< Interrupt When TC matches MR0 */
#define MR0R				(1<<1)	/**< Reset TC when TC matches MR0 */

#define TIMER_0		0	/**< Timer 0 */
#define TIMER_1		1	/**< Timer 1 */
#define MODE_CAP_RISE	0	/**< Captura por flanco ascendente */
#define MODE_CAP_FALL	1	/**< Captura por flanco descendente */
#define MODE_CAP_BOTH	2	/**< Captura por flanco ascendente y descendente */
#define MODE_TEMP			3	/**< Temporizador */

/**
  * @brief  Interrupcion de Timer 1 - Timer 1 interruption
  * @param  
  * @param  
  * @retval 
  */
__weak void INTERRUPT_TIMER1 (void) __irq;

/**
  * @brief  Interrupcion de Timer 0 - Timer 0 interruption
  * @param  
  * @param  
  * @retval
	* @details
  */
__weak void INTERRUPT_TIMER0 (void) __irq;

/**
  * @brief  Inicializacion para cada timer, configurables en cualquier momento
  * @param  prior	Prioridad de interrupcion elegida por el usuario
  * @param  timer	Selecciona un timer especifico
  * @param  mode	Elige en que modo usar el timer
	* @param  pin		Vincula la configuracion a un pin especifico
	* @param  temp	Para capturas establece la resolucion en multiples de 50nseg (maximo 1431655765 para 60MHz, aprox71,583seg);
	*								para temporizadores establece el tiempo de ejecucion.
	*								No poner ni 0 ni numeros negativos.
	* @retval
	* @details	Para configurar Capturas hace falta llamar a la funcion para cada canal
	* @todo		assert para prior, assert para timer, assert para mode, assert para pin, assert para temp
  */
void init_Timer (uint32_t prior, uint32_t timer, uint32_t mode, uint32_t pin, uint32_t temp);

/**
  * @brief  Inicializacion de Timer 0 - Timer 0 initialization
  * @param  
  * @param  
  * @retval
	* @details	Configuracion como Capture Signal - Capture Signal configuration
  */
void init_T0(void);

/**
  * @brief  Inicializacion de Timer 1 - Timer 1 initialization
  * @param  
  * @param  
  * @retval
	* @details Configuracion como temporizador con recarga automatica - Automatic recharged timer configuration
  */
void init_T1(void);

/**
  * @brief  Inicializacion del PLL - PLL initialization
  * @param  
  * @param  
  * @retval
	* @details
  */
void init_pll (void);

#endif	// End _LPC2148_HAL_TIM_H
