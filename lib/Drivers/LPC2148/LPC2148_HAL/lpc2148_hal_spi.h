/**
  ******************************************************************************
  * @file    lpc2148_hal_spi.h
  * @author  Alejandro Gast�n Alvaerez
  * @version 1.0
  * @date    09/01/2016
  * @brief   SPI Hardware Abstraction Layer (HAL)
  ******************************************************************************
  * @attention
  *
  ******************************************************************************
	*/

#ifndef _LPC2148_HAL_SPI_H
#define _LPC2148_HAL_SPI_H


#include <lpc2148_hal.h>


typedef enum{
	SPI_Slave  = 0,
	SPI_Master = 1,
}SPI_Modes;

typedef enum{
	SPI_CLK_POL_HIGH = 0,
	SPI_CLK_POL_LOW  = 1,
}SPI_CLK_Pol;

typedef enum{
	SPI_PH_FRST = 0,
	SPI_PH_SCND = 1,
}SPI_PHASE;

typedef enum{
	SPI_MSB_FIRST = 0,
	SPI_LSB_FIRST = 1,
}SPI_Tx_ORDER;

typedef enum{
	SPI_IRQ_DISABLE = 0,
	SPI_IRQ_ENABLE  = 1,
}SPI_IRQ;

typedef enum{
	SPI_Tx08 = 0x08,
	SPI_Tx09 = 0x09,
	SPI_Tx10 = 0x0A,
	SPI_Tx11 = 0x0B,
	SPI_Tx12 = 0x0C,
	SPI_Tx13 = 0x0D,
	SPI_Tx14 = 0x0E,
	SPI_Tx15 = 0x0F,
	SPI_Tx16 = 0x00,
}SPI_TxBits;

typedef enum{
	SPI_OK = 0,
	SPI_WAIT = 1,
	
	SPI_ABORT = 0x08,
	SPI_MODE_FAULT = 0x10,
	SPI_READ_OVERRUN = 0x20,
	SPI_WRITE_COLLISION = 0x40,
	SPI_Tx_COMPLETE = 0x80,
}SPI_Status;


/**
 * @brief Configures the SPI.
 * @details Configures the Serial Port Interface (SPI) 0.
 * 
 * @param prescaler Value which will be used to calculate SPI frequency (SPI_freq = PCLK / prescaler). Must be an even value greater than 8.
 * @param mode Sets the SPI mode that will be used (Master/Slave).
 * @param clkpol Sets the Clock Polarization.
 * @param phase Sets the Clock Phase.
 * @param bitorder Sets the bit order transmission (MSB first or LSB first).
 * @param irq Enables the interrupt for the SPI.
 * @param num_bits Sets the number of bits to be send in each transmittion.
 * @return returns SPI frequency.
 */
int initSPI(char prescaler, SPI_Modes mode, SPI_CLK_Pol clkpol, SPI_PHASE phase, SPI_Tx_ORDER bitorder, SPI_IRQ irq, SPI_TxBits num_bits);
/**
 * @brief Restores SPI configuration.
 * @details Sets SPI configuration to default (reset) state.
 * @return returns SPI status.
 */
SPI_Status deinitSPI(void);
/**
 * @brief Gets SPI status.
 * @details Consults and returns SPI status.
 * @return returns SPI frequency.
 */
SPI_Status statusSPI (void);
/**
 * @brief Writes the SPI buffer.
 * @details Writes the SPI buffer. When the SPI is in Master mode, this automatically starts an SPI communication.
 * 
 * @param data Data to be transmitted.
 */
SPI_Status writeSPI (int data);
/**
 * @brief Reads the SPI buffer.
 * @details Reads the SPI buffer.
 * 
 * @param data Pointer to a variable where the data should be stored.
 */
SPI_Status readSPI (int *data);


#endif	// End _LPC2148_HAL_SPI_H
