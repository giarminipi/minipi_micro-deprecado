/**
  ******************************************************************************
  * @file    IR_sensor.h
  * @author  
  * @version 
  * @date    
  * @brief   
  ******************************************************************************
  * @attention
  *
  ******************************************************************************
	*/

/**
  ******************************************************************************
  * @file    	IR_sensor.h
  * @author		Alejandro Gast�n Alvarez
  * @version	1.0
  * @date			08/10/2016
  * @brief		Programa de testeo de los sensores de l�nea
  ******************************************************************************
  * @attention
  *
  ******************************************************************************
  */
  
	
#ifndef _IR_SENSOR_H_
#define _IR_SENSOR_H_

/* Includes ------------------------------------------------------------------*/
#include <lpc2148_hal.h>

typedef enum
{
	IR_1 = PIN_21,
	IR_2 = PIN_23,
	IR_3 = PIN_24,
	IR_4 = PIN_25,
}IR_LINEs;


typedef union
{
	uint8_t line_sensor;

	struct
  {
    unsigned char s1 : 1;
    unsigned char s2 : 1;
    unsigned char s3 : 1;
    unsigned char s4 : 1;
  }ir;
	
}IR_Line_Data;

void initIrLinea (void);
IR_Line_Data get_ir_line_data (void);

#endif	// End _IR_SENSOR_H_
