/**
  ******************************************************************************
  * @file    infrared.c
  * @author  
  * @version 
  * @date    
  * @brief   
  ******************************************************************************
  * @attention
  *
  ******************************************************************************
	*/
	
#include <ir_line.h>


void initIrLinea (void)
{
	P1ModeSelec (IR_1 | IR_2 | IR_3 | IR_4, P1_GPIO_DEBUG, 0);
}

IR_Line_Data get_ir_line_data (void)
{
	IR_Line_Data sensor_status;
	
	sensor_status.line_sensor = 0;
	
	sensor_status.line_sensor |= Pin_Read (PORT_1, IR_1) << 0;
	sensor_status.line_sensor |= Pin_Read (PORT_1, IR_2) << 1;
	sensor_status.line_sensor |= Pin_Read (PORT_1, IR_3) << 2;
	sensor_status.line_sensor |= Pin_Read (PORT_1, IR_4) << 3;
	
	return sensor_status;
}

