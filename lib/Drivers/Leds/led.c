/**
  ******************************************************************************
  * @file    led.c
  * @author  
  * @version 
  * @date    
  * @brief   
  ******************************************************************************
  * @attention
  *
  ******************************************************************************
	*/
	
#include <led.h>

void initLed (void)
{
	P0ModeSelec (LED_1 | LED_2 |LED_3, P0_MODE_GPIO, 1);
}

void ledOn (LEDs led)
{
	Pin_Set (PORT_0, (uint32_t) led);
}

void ledOff (LEDs led)
{
	Pin_Clear (PORT_0, (uint32_t) led);
}

void ledToggle (LEDs led)
{
	Pin_Toggle (PORT_0, (uint32_t) led);
}
