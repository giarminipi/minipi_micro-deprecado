/**
  ******************************************************************************
  * @file    led.h
  * @author  
  * @version 
  * @date    
  * @brief   
  ******************************************************************************
  * @attention
  *
  ******************************************************************************
	*/

#ifndef _LED_H_
#define _LED_H_

#include <lpc2148_hal.h>

//#define     LED0        PIN_3 /* binary: 00000000_00000000_00000000_00001000 */
//#define     LED1        PIN_4 /* binary: 00000000_00000000_00000000_00010000 */
//#define     LED2        PIN_5 /* binary: 00000000_00000000_00000000_00100000 */

typedef enum
{
	LED_1 = PIN_3,
	LED_2 = PIN_4,
	LED_3 = PIN_5,
}LEDs;

void initLed (void);
void ledOn (LEDs led);
void ledOff (LEDs led);
void ledToggle (LEDs led);


#endif	// End _LED_H_
