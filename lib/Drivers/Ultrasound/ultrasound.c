/**
  ******************************************************************************
  * @file    ultrasound.c
  * @author  JMGG & JSTC & AB
  * @version 4.0
  * @date    29/03/2016
  * @brief   Ultrasound function package
  ******************************************************************************
  * @attention
  *
  ******************************************************************************
	*/
	
#include <ultrasound.h>

struct sVariables_US {
	uint32_t buffer_C0_up;
	uint32_t buffer_C0_dw;
	uint32_t buffer_C2_up;
	uint32_t buffer_C2_dw;
	
	uint32_t resolution_US;
	
	uint32_t tics_US_1;
	uint32_t tics_US_2;
	uint32_t tics_US_3;
	uint32_t tics_US_4;
};

struct sVariables_US VarUS;

struct sVariables_TM {
	uint32_t buffer_tiempo_us_gestor;
	uint32_t buffer_tiempo_us1; 
	uint32_t buffer_tiempo_us2;
	uint32_t buffer_tiempo_us3;
	uint32_t buffer_tiempo_us4;
};

struct sVariables_TM VarTM;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
int set_resolution_US (uint32_t R)
{
	if(5000<R || R<1)
	{
		R = (-1);
	}
	return R;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void init_Sensores_US ()
{
    VarUS.buffer_C0_up = 0;
		VarUS.buffer_C0_dw = 0;
		VarUS.buffer_C2_up = 0;
		VarUS.buffer_C2_dw = 0;
		VarUS.tics_US_1 = 0;
		VarUS.tics_US_2 = 0;
		VarUS.tics_US_3 = 0;
		VarUS.tics_US_4 = 0;
	
		VarUS.resolution_US = (uint32_t)set_resolution_US(58);
		//TODO: assert
	
		Trigger1_Output;
  	Trigger2_Output;
  	Trigger3_Output;
  	Trigger4_Output;
}



////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void sensar_US_1 (void)
{
	static int estado_US_1 = trigger;
	static uint32_t buffer_T1 =0;
			
	switch (estado_US_1){
			///////////////////////////////////////////////////////////////////////////
			case trigger:
			
				if(VarTM.buffer_tiempo_us1==0){
					VarTM.buffer_tiempo_us1 = 39;
					Trigger1_ON;
				}
				else if(VarTM.buffer_tiempo_us1==38){
					Trigger1_OFF;
					estado_US_1 = obtener_t1;
				}
				break;
			///////////////////////////////////////////////////////////////////////////
			case obtener_t1:
				
				if(VarUS.buffer_C2_up){
					buffer_T1 = VarUS.buffer_C2_up;
					VarUS.buffer_C2_up = 0;
					estado_US_1 = obtener_t2;
				}
				if(VarTM.buffer_tiempo_us1==0){// No hay sensor
					//assert - TODO
					estado_US_1 = trigger;
				}
				break;
			///////////////////////////////////////////////////////////////////////////
			case obtener_t2:
				
				if(VarUS.buffer_C2_dw){
					VarUS.tics_US_1= VarUS.buffer_C2_dw - buffer_T1;
					VarUS.buffer_C2_dw = 0;
					estado_US_1 = trigger;
				}
				break;
			///////////////////////////////////////////////////////////////////////////
			case recarga:
				
				if(VarTM.buffer_tiempo_us1==0){
					estado_US_1 = trigger;
				}
				break;			
			}
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void sensar_US_3 (void)
{
	static int estado_US_3 = trigger;
	static uint32_t buffer_T1 =0;
		
	switch (estado_US_3){
			///////////////////////////////////////////////////////////////////////////
			case trigger:
			
				if(VarTM.buffer_tiempo_us3==0){
					VarTM.buffer_tiempo_us3 = 39;
					Trigger3_ON;
				}
				else if(VarTM.buffer_tiempo_us3==38){
					Trigger3_OFF;
					estado_US_3 = obtener_t1;
				}
				break;
			///////////////////////////////////////////////////////////////////////////
			case obtener_t1:
				
				if(VarUS.buffer_C0_up){
					buffer_T1 = VarUS.buffer_C0_up;
					VarUS.buffer_C0_up = 0;
					estado_US_3 = obtener_t2;
				}
				if(VarTM.buffer_tiempo_us3==0){// No hay sensor
					//assert - TODO
					estado_US_3 = trigger;
				}
				break;
			///////////////////////////////////////////////////////////////////////////
			case obtener_t2:
				
				if(VarUS.buffer_C0_dw){
					VarUS.tics_US_3= VarUS.buffer_C0_dw - buffer_T1;
					VarUS.buffer_C0_dw = 0;
					estado_US_3=trigger;
				}
				break;
			///////////////////////////////////////////////////////////////////////////
			case recarga:
				
				if(VarTM.buffer_tiempo_us3==0){
					estado_US_3 = trigger;
				}
				break;			
			}
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void sensar_US_2 (void)
{
	static int estado_US_2 = trigger;
	static uint32_t buffer_T1 =0;
	
	switch (estado_US_2){
			///////////////////////////////////////////////////////////////////////////
			case trigger:
			
				if(VarTM.buffer_tiempo_us2==0){
					VarTM.buffer_tiempo_us2 = 39;
					Trigger2_ON;
				}
				else if(VarTM.buffer_tiempo_us2==38){
					Trigger2_OFF;
					estado_US_2 = obtener_t1;
				}
				break;
			///////////////////////////////////////////////////////////////////////////
			case obtener_t1:
				
				if(VarUS.buffer_C2_up){
					buffer_T1 = VarUS.buffer_C2_up;
					VarUS.buffer_C2_up = 0;
					estado_US_2 = obtener_t2;
				}
				if(VarTM.buffer_tiempo_us2==0){// No hay sensor
					//assert - TODO
					estado_US_2 = trigger;
				}
				break;
			///////////////////////////////////////////////////////////////////////////
			case obtener_t2:
				
				if(VarUS.buffer_C2_dw){
					VarUS.tics_US_2 = VarUS.buffer_C2_dw - buffer_T1;
					VarUS.buffer_C2_dw = 0;
					estado_US_2=trigger;
				}
				break;
			///////////////////////////////////////////////////////////////////////////
			case recarga:
				
				if(VarTM.buffer_tiempo_us2==0){
					estado_US_2 = trigger;
				}
				break;			
			}
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void sensar_US_4 (void)
{
	static int estado_US_4 = trigger;
	static uint32_t buffer_T1 = 0;
	
	switch (estado_US_4){
			///////////////////////////////////////////////////////////////////////////
			case trigger:
			
				if(VarTM.buffer_tiempo_us4==0){
					VarTM.buffer_tiempo_us4 =39;
					Trigger4_ON;
				}
				else if(VarTM.buffer_tiempo_us4==38){
					Trigger4_OFF;
					estado_US_4 = obtener_t1;
				}
				break;
			///////////////////////////////////////////////////////////////////////////
			case obtener_t1:
				
				if(VarUS.buffer_C0_up){
					buffer_T1 = VarUS.buffer_C0_up;
					VarUS.buffer_C0_up = 0;
					estado_US_4 = obtener_t2;
				}
				if(VarTM.buffer_tiempo_us4==0){// No hay sensor
					//assert
					estado_US_4 = trigger;
				}
				break;
			///////////////////////////////////////////////////////////////////////////
			case obtener_t2:
				
				if(VarUS.buffer_C0_dw){
					VarUS.tics_US_4 = VarUS.buffer_C0_dw - buffer_T1;
					VarUS.buffer_C0_dw = 0;
					estado_US_4 = trigger;
				}
				break;
			///////////////////////////////////////////////////////////////////////////
			case recarga:
				
				if(VarTM.buffer_tiempo_us4==0){
					estado_US_4 = trigger;
				}
				break;			
			}
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void sensar_US (void)
{
	static int estado_us_gestor = inicio;
	switch (estado_us_gestor ){
////////////////////////////////////////////////////////////////////////////////////////
		case inicio:
						//es el estado que ingresa la primera vez, solo deberia pasar una sola vez por aqui
			VarTM.buffer_tiempo_us1 = 0;
		  VarTM.buffer_tiempo_us3 = 0;
			VarTM.buffer_tiempo_us_gestor =60;
		  estado_us_gestor = sensar1_3;
			sensar_US_1();
			sensar_US_3();
		  break;
////////////////////////////////////////////////////////////////////////////////////////	
		case sensar1_3:
			if(VarTM.buffer_tiempo_us_gestor){
				sensar_US_1();
				sensar_US_3();
			}
			else{
							//entra si se cumplieron los primeros 40ms, inicializo para que sensen 2 y 4
				VarTM.buffer_tiempo_us2 = 0;
				VarTM.buffer_tiempo_us4 = 0;
				VarTM.buffer_tiempo_us_gestor =60;
				estado_us_gestor = sensar2_4;
				sensar_US_2();
				sensar_US_4();
			}
			break;
////////////////////////////////////////////////////////////////////////////////////////			  
		case sensar2_4:
			if(VarTM.buffer_tiempo_us_gestor){
				sensar_US_2();
		    sensar_US_4();
			}
			else{		
							//entra si se cumplieron los segundos 40ms, inicializo para que sensen 1 y 3
				VarTM.buffer_tiempo_us1 = 0;
				VarTM.buffer_tiempo_us3 = 0;
				VarTM.buffer_tiempo_us_gestor =60;
				estado_us_gestor = sensar1_3;  
				sensar_US_1();
				sensar_US_3();
			}
			break;
////////////////////////////////////////////////////////////////////////////////////////	
		default:
						//por si acaso, entra, inicializa los timers y va al estado de inicio
			VarTM.buffer_tiempo_us1 = 0;
		  VarTM.buffer_tiempo_us3 = 0;
		  VarTM.buffer_tiempo_us2 = 0;
		  VarTM.buffer_tiempo_us4 = 0;
		  estado_us_gestor = inicio;
		  break;		
////////////////////////////////////////////////////////////////////////////////////////	
	}
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
US_Data get_US_data (uint32_t cota_inf, uint32_t cota_sup)
{
	US_Data sensor_status;
	uint32_t Lectura;
	
	sensor_status.US_sensor = 0;
	
	Lectura = read_US_1() / VarUS.resolution_US;
	if(cota_inf < Lectura && Lectura < cota_sup)
	{
		sensor_status.US_sensor |= (1<<0);
	}
	
	Lectura = read_US_2() / VarUS.resolution_US;
	if(cota_inf < Lectura && Lectura < cota_sup)
	{
		sensor_status.US_sensor |= (1<<1);
	}
	
	Lectura = read_US_3() / VarUS.resolution_US;
	if(cota_inf < Lectura && Lectura < cota_sup)
	{
		sensor_status.US_sensor |= (1<<2);
	}
	
	Lectura = read_US_4() / VarUS.resolution_US;
	if(cota_inf < Lectura && Lectura < cota_sup)
	{
		sensor_status.US_sensor |= (1<<3);
	}

	return sensor_status;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
uint32_t read_US_1 (void)
{
		return (VarUS.tics_US_1);
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
uint32_t read_US_2 (void)
{
		return (VarUS.tics_US_2);
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
uint32_t read_US_3 (void)
{
		return (VarUS.tics_US_3);
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
uint32_t read_US_4 (void)
{
		return (VarUS.tics_US_4);
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void INTERRUPT_TIMER0 (void) __irq 
{
// Pin_Read(puerto, pin)
	  uint32_t estado_actual_eco_1o2;
	  uint32_t estado_actual_eco_3o4;
		static uint32_t estado_anterior_eco_1o2 = 0;     
	  static uint32_t estado_anterior_eco_3o4 = 0; 
	
    estado_actual_eco_1o2 = Pin_Read(PORT_0,echo_1_2);
	  estado_actual_eco_3o4 = Pin_Read(PORT_0,echo_3_4);
		
	if ( estado_actual_eco_1o2 != estado_anterior_eco_1o2){
		//entra aqui si el pin que vario es el del eco_1_2
		if(estado_actual_eco_1o2){
			//entra por flanco ascendente 
			VarUS.buffer_C2_up = T0CR2;
		}
		else{
			//entra por flanco descendente
			VarUS.buffer_C2_dw = T0CR2;			
		}
		estado_anterior_eco_1o2 = estado_actual_eco_1o2;
	}
  	
	if ( estado_actual_eco_3o4 != estado_anterior_eco_3o4){
		//entra aqui si el pin que vario es el del eco_3_4
		
		if(estado_actual_eco_3o4){
			//entra por flanco ascendente 
			VarUS.buffer_C0_up = T0CR0;
		}
		else{
			//entra por flanco descendente
			VarUS.buffer_C0_dw = T0CR0;			
		}
		estado_anterior_eco_3o4 = estado_actual_eco_3o4;
	}  
  		
		// Limpia la int
	T0IR |= 0x00000010;	// Limpia todos los flags de interrupcion del T0IR
	VICVectAddr = 0x00000000;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void INTERRUPT_TIMER1 (void) __irq 
{
	if(VarTM.buffer_tiempo_us_gestor) VarTM.buffer_tiempo_us_gestor--;
  if(VarTM.buffer_tiempo_us1) VarTM.buffer_tiempo_us1--;
	if(VarTM.buffer_tiempo_us2) VarTM.buffer_tiempo_us2--;
	if(VarTM.buffer_tiempo_us3) VarTM.buffer_tiempo_us3--;
	if(VarTM.buffer_tiempo_us4) VarTM.buffer_tiempo_us4--;
	
	// Limpia la int
	T1IR |= 0x00000010;	// Limpia todos los flags de interrupcion del T1IR
	VICVectAddr = 0x00000000;
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
