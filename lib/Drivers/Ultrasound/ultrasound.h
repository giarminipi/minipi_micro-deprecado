/**
  ******************************************************************************
  * @file    ultrasound.h
  * @author  JMGG & JSTC & AB
  * @version 4.0
  * @date    29/03/2016
  * @brief   Ultrasound function package
  ******************************************************************************
  * @attention
  *
  ******************************************************************************
	*/

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef _ULTRASOUND_H_
#define _ULTRASOUND_H_

/* Includes ------------------------------------------------------------------*/
#include <lpc2148_hal.h>
#include <led.h>


#define Trigger1				PIN_23	/**< Pin del disparador del Ultrasonido 1: P0.23 */ /**< Ultrasound trigger's pin 1: P0.23 */
#define Trigger1_Output	P0ModeSelec (Trigger1, P0_MODE_GPIO, 1) /**< Configura al pin del Trigger1 como salida */ /**< Configures the Trigger1's pin as output*/
#define Trigger1_ON			Pin_Set (PORT_0, (uint32_t) Trigger1)		/**< Enciende al pin del Trigger1 */ /**< Set Trigger1's pin on */
#define Trigger1_OFF		Pin_Clear (PORT_0, (uint32_t) Trigger1)	/**< Apaga el pin del Trigger1 */ /**< Set Trigger1's pin off */

#define Trigger2				PIN_21 	/**< Pin del disparador del Ultrasonido 2: P0.21 */ /**< Ultrasound trigger's pin 2: P0.25 */
#define Trigger2_Output	P0ModeSelec (Trigger2, P0_MODE_GPIO, 1) /**< Configura al pin del Trigger2 como salida */ /**< Configures the Trigger2's pin as output*/
#define Trigger2_ON			Pin_Set (PORT_0, (uint32_t) Trigger2)		/**< Enciende al pin del Trigger2 */ /**< Set Trigger2's pin on */
#define Trigger2_OFF		Pin_Clear (PORT_0, (uint32_t) Trigger2)	/**< Apaga el pin del Trigger2 */ /**< Set Trigger2's pin off */

#define Trigger3				PIN_25	/**< Pin del disparador del Ultrasonido 3: P0.25 */ /**< Ultrasound trigger's pin 3: P0.25 */
#define Trigger3_Output	P0ModeSelec (Trigger3, P0_MODE_GPIO, 1) /**< Configura al pin del Trigger3 como salida */ /**< Configures the Trigger3's pin as output*/
#define Trigger3_ON			Pin_Set (PORT_0, (uint32_t) Trigger3)		/**< Enciende al pin del Trigger3 */ /**< Set Trigger3's pin on */
#define Trigger3_OFF		Pin_Clear (PORT_0, (uint32_t) Trigger3)	/**< Apaga el pin del Trigger3 */ /**< Set Trigger3's pin off */

#define Trigger4				PIN_16	/**< Pin del disparador del Ultrasonido 4: P0.16 */ /**< Ultrasound trigger's pin 4: P0.16  */
#define Trigger4_Output	P0ModeSelec (Trigger4, P0_MODE_GPIO, 1)	/**< Configura al pin del Trigger4 como salida */ /**< Configures the Trigger4's pin as output*/
#define Trigger4_ON			Pin_Set (PORT_0, (uint32_t) Trigger4)		/**< Enciende al pin del Trigger4 */ /**< Set Trigger4's pin on */
#define Trigger4_OFF		Pin_Clear (PORT_0, (uint32_t) Trigger4)	/**< Apaga el pin del Trigger4 */ /**< Set Trigger4's pin off */

#define echo_1_2        PIN_6   /**< Pin del eco de los Ultrasonido 1 y 2: P0.6 - J11.4 */ /**< Ultrasound eco's pin 1 and 2: P0.6 - J11.4 */
#define echo_3_4        PIN_22  /**< Pin del eco de los Ultrasonido 3 y 4: P0.22 - J9.5 */ /**< Ultrasound eco's pin 3 and 4: P0.22 - J9.5 */


/**
  * @typedef US_states
  * @brief	 Estados posibles para las funciones sensar_US_x (para x perteneciente a {1,2,3,4})
	*					 Possible states for sensar_US_x functions (for x belong to {1,2,3,4})
  */
typedef enum {
	trigger,
	obtener_t1,
	obtener_t2,
	recarga
} US_states;

/**
  * @typedef US_gestor_states
  * @brief	 Estados posibles para la funcion sensar_US
	*					 Possible states for sensar_US function
  */
typedef enum {
	inicio,
	sensar1_3,
	sensar2_4
} US_gestor_states;

/**
  * @typedef US_Data
  * @brief	 Dato que indica si hay objeto dentro de la ventana de medicion
	*					 Data that indicates if there is an object inside the meassure window
  */
typedef union
{
	uint8_t US_sensor;

	struct
  {
    unsigned char s1 : 1;
    unsigned char s2 : 1;
    unsigned char s3 : 1;
    unsigned char s4 : 1;
  }us;
	
}US_Data;

/**
  * @brief  Instancia global restringida al archivo con la informacion de los sensores
  * @retval Devuelve la direccion de la estructura con tal informacion
  */
struct sVariables_US *variables_US(void);

/**
  * @brief  Instancia global restringida al archivo con la informacion de Time Machine del ultrasonido
  * @retval Devuelve la direccion de la estructura con tal informacion
  */
struct sVariables_TM *variables_TM(void);

/**
  * @brief  Fija la resolucion de los Ultrasonidos
  * @retval Devuelve (-1) si la resolucion es erronea
  */
int set_resolution_US (uint32_t R);

/**
  * @brief  Inicializacion de los triggers de todos los canales
	*					Trigger's initialization for every channel
  * @param  
  * @param  
  * @retval 
  */
void init_Sensores_US (void);

/**
  * @brief  Gestiona y obtiene mediciones del Ultrasonido 1
	*					Manages and obtains Ultrasound 1 meassures
  * @param  
  * @retval 
  */
void sensar_US_1 (void);

/**
  * @brief  Gestiona y obtiene mediciones del Ultrasonido 2
	*					Manages and obtains Ultrasound 2 meassures
  * @param  
  * @retval 
  */
void sensar_US_2 (void);

/**
  * @brief  Gestiona y obtiene mediciones del Ultrasonido 3
	*					Manages and obtains Ultrasound 3 meassures
	* @param  
  * @param  
  * @retval 
  */
void sensar_US_3 (void);

/**
  * @brief  Gestiona y obtiene mediciones del Ultrasonido 4
	*					Manages and obtains Ultrasound 4 meassures
  * @param  
  * @param  
  * @retval 
  */
void sensar_US_4 (void);

/**
  * @brief  Gestiona todos los sensores posibles
	*					Manages every possible sensor
  * @param  
  * @param  
  * @retval 
	* @details
	* | Ubicacion | Eje A | Eje B |			| Location | Axis A | Axis B |
	* | :-------: | :---: | :---: |			| :------: | :----: | :----: |
	* | 1         | 1     |       |			| 1        | 1      |        |
	* | 4+2       | +     | 4+2   |			| 4+2      | +      | 4+2    |
	* | 3         | 3     |       |			| 3        | 3      |        |
	* Ubicacion de los sensores, se activan conjuntamente los sensores opuestos, 1 y 3 luego 2 y 4, luego 1 y 3, cada 40ms
	* 1 y 2 van a un pin de interrupcion de captura, 3 y 4 van a otro pin de interrupcion de captura 
	* Sensor's location; opposites sensors activate together; 1 and 3 first, 2 and 4 later, then back to 1 and 3, each 40ms
	* 1 and 2 link to a capture interrupt pin, 3 and 4 link to another
	*/
void sensar_US (void);

/**
  * @brief  Dicta si los sensores tienen un obstaculo dentro de un rango
	*					Says if sensors have an obstacule inside the range
  * @param  cota_inf Cota inferior de la ventana; debe ser superior a 20mm
  * @param  cota_sup Cota superior de la ventana; debe ser superior a 4000mm
  * @retval Devuelve una union que indica con cada bit si hay o no obstaculo asociado
	*/
US_Data get_US_data (uint32_t cota_inf, uint32_t cota_sup);

/**
  * @brief  Obtiene mediciones del Ultrasonido 1
  * @retval 
  */
uint32_t read_US_1 (void);

/**
  * @brief  Obtiene mediciones del Ultrasonido 2
  * @retval 
  */
uint32_t read_US_2 (void);

/**
  * @brief  Obtiene mediciones del Ultrasonido 3
  * @retval 
  */
uint32_t read_US_3 (void);

/**
  * @brief  Obtiene mediciones del Ultrasonido 4
  * @retval 
  */
uint32_t read_US_4 (void);

/**
  * @brief  Interrupcion de Timer 0
	*					Timer 0 interruption
  * @param  
  * @param  
  * @retval
	* @details Configurado para Capture Signal: C0.0 y C0.2
	*					 Configured for Capture Signal: C0.0 and C0.2
  */
void INTERRUPT_TIMER0 (void) __irq;

#endif	// End _ULTRASOUND_H_
