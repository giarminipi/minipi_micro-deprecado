/**
  ******************************************************************************
  * @file    ring_bufffer.c
  * @author  Alejandro Alvarez
  * @version V1.0.0
  * @date    19-Mayo-2016
  * @brief   Audio Functions for ASR_DTW
  ******************************************************************************
  * @attention
  *
  ******************************************************************************
*/

/* Includes ------------------------------------------------------------------*/
#include <ring_buffer.h>
#include <string.h>
#include <stdlib.h>
#include <assert.h>


RingBufferStatus ringBuf_init ( RingBuffer* buffer, const size_t buff_size, bool can_override )
{
    // Alloco memoria para el bufffer
    buffer->buff = malloc ( buff_size );

    // Inicializo las variables
    buffer->head = buffer->buff;
    buffer->buff_size = buff_size;
    buffer->can_override = can_override;
    buffer->num_clients = 0;
    buffer->clients = NULL;
    buffer->count = 0;
    buffer->last_given_client_id = 0;

    // Creo el mutex
    mtx_init ( &buffer->rw_mutex, mtx_plain );

    return BUFF_OK;
}

RingBufferStatus ringBuf_deinit ( RingBuffer *buffer )
{
	if (has_ringBuf_clients(buffer))
		return BUFF_WAITING_FOR_CLIENTS;

	//Bloqueo el mutex
    mtx_lock ( &buffer->rw_mutex );

    free ( buffer->buff );
    buffer->buff  = NULL;

    free ( buffer->clients );
    buffer->clients = NULL;

    buffer->buff_size = 0;
    buffer->head = NULL;
    buffer->count = 0;
    buffer->last_given_client_id = 0;

    // Libero el mutex
    mtx_unlock ( &buffer->rw_mutex );

    mtx_destroy ( &buffer->rw_mutex );

    return BUFF_OK;
}

RingBufferStatus ringBuf_flush ( RingBuffer *buffer )
{
    //Bloqueo el mutex
    mtx_lock ( &buffer->rw_mutex );

    buffer->count = 0;
    buffer->head	 = buffer->buff;

    for ( uint32_t i=0; i<buffer->num_clients; i++ ) {
        buffer->clients[i].count = 0;
        buffer->clients[i].ptr = buffer->buff;
    }

    // Libero el mutex
    mtx_unlock ( &buffer->rw_mutex );

    return BUFF_OK;
}

RingBufferStatus ringBuf_registClient ( RingBuffer* buffer, size_t read_size, size_t shift_size, callback client_func, uint32_t* client_id )
{
    if (!is_ringBuf_init(buffer))
     return BUFF_ERROR;

	//Bloqueo el mutex
	mtx_lock ( &buffer->rw_mutex );

	// Me fijo que lo que quiera leer no sea mayor al tama�o del buffer
	assert ( buffer->buff_size >= read_size );
	assert ( buffer->buff_size >= shift_size );

	// Incremento la cantidad de clientes
	buffer->num_clients++;

	// Le asigno un n�mero de cliente
	*client_id = ++buffer->last_given_client_id;

	// Realoco la memoria para que tenga el tama�o suficiente para el n�evo n�mero de clientes
	//     ringBufClient *aux = malloc ( buffer->num_clients * sizeof ( ringBufClient ) );              // Alloco nueva memoria para los clientes
	//     memcpy ( aux, buffer->clients, ( buffer->num_clients-1 ) * sizeof ( ringBufClient ) );        // Copio todos los clientes viejos
	//     free ( buffer->clients );                                                                    // Eliminio la memoria anterior, si hab�a algo alocado (si no hay nada buffer->clients = NULL)
	//     buffer->clients = aux;                                                                       // Guardo el puntero a la nueva memoria

	// Realoco la memoria para que tenga el tama�o suficiente para el n�evo n�mero de clientes
	if ( buffer->num_clients > 1 )
		buffer->clients = realloc ( buffer->clients, buffer->num_clients * sizeof ( RingBufferClient ) );
	else
		buffer->clients = malloc ( sizeof ( RingBufferClient ) );

	// Inicializo las variables del cliente
	buffer->clients[buffer->num_clients-1].client_id = *client_id;
	buffer->clients[buffer->num_clients-1].count = 0;
	buffer->clients[buffer->num_clients-1].ptr = buffer->head;
	buffer->clients[buffer->num_clients-1].overrun = false;
	buffer->clients[buffer->num_clients-1].read_size = read_size;
	buffer->clients[buffer->num_clients-1].shift_size = shift_size;
	buffer->clients[buffer->num_clients-1].client_func = client_func;

	// Libero el mutex
	mtx_unlock ( &buffer->rw_mutex );

	return BUFF_OK;
}

RingBufferStatus ringBuf_unregistClient ( RingBuffer *buffer, const uint8_t client_num )
{
    uint32_t client_idx;

    //Bloqueo el mutex
    mtx_lock ( &buffer->rw_mutex );


    if ( buffer->num_clients > 1 )
    {
        // Busco el �ndice de este cliente
        ringBuf_findClient ( buffer, client_num, &client_idx );

        // Creo un nuevo espacio de memoria con un cliente menos
        RingBufferClient *aux = malloc ( ( buffer->num_clients-1 ) * sizeof ( RingBufferClient ) );

        // Copio la info de todos los clientes menos del que estoy des-registrando
        memcpy ( aux, buffer->clients, client_idx * sizeof ( RingBufferClient ) );                                                                  //Copio los que estan antes
        memcpy ( &aux[client_idx], &buffer->clients[client_idx+1], ( buffer->num_clients - ( client_idx+1 ) ) * sizeof ( RingBufferClient ) );       //Copio los que estan despu�s

        // libero la memoria vieja
        free ( buffer->clients );
        buffer->clients = aux;

    }
    else
    {
        free ( buffer->clients );
        buffer->clients = NULL;
    }

    // Decremento la cantidad de cliente
    buffer->num_clients--;

    // Actualizo la cuenta
    ringBuf_updateCount ( buffer );

    // Libero el mutex
    mtx_unlock ( &buffer->rw_mutex );

    return BUFF_OK;
}

RingBufferStatus ringBuf_shift_Client ( RingBuffer *buffer, const uint32_t client_id, size_t shift_size )
{
    uint32_t idx;
    RingBufferStatus status = BUFF_OK;

    //Bloqueo el mutex
    mtx_lock ( &buffer->rw_mutex );

    // Busco el cliente
    status = ringBuf_findClient ( buffer, client_id, &idx );
    if ( status == BUFF_OK )
    {
        // Me fijo que lo que quiera shiftear sea menor que lo que tiene para leer
        assert ( shift_size < buffer->clients[idx].count );

        // Obtengo el espacio que queda hacia la derecha del buffer
        uint32_t right_space = buffer->buff_size - ( buffer->clients[idx].ptr - buffer->buff );

        // Actualizo el puntero
        if ( shift_size < right_space )
            buffer->clients[idx].ptr += shift_size;
        else
            buffer->clients[idx].ptr = &buffer->buff[ shift_size - right_space ];

        // Decremento el contador
        buffer->clients[idx].count -= shift_size;
    }

    // Actualizo la cuenta
    ringBuf_updateCount ( buffer );

    // Libero el mutex
    mtx_unlock ( &buffer->rw_mutex );

    return status;
}

inline bool is_ringBuf_init	( RingBuffer *buffer )
{
    return buffer->buff_size != 0;
}

inline bool has_ringBuf_clients	( RingBuffer *buffer )
{
    return buffer->num_clients != 0;
}

inline bool is_ringBuf_empty ( RingBuffer *buffer )
{
    return 0 == buffer->count;
}

inline bool is_ringBuf_full ( RingBuffer *buffer )
{
    return ( buffer->count >= buffer->buff_size );
}

size_t ringBuf_available_data(RingBuffer* buffer, uint32_t client_id)
{
 uint32_t idx;
 ringBuf_findClient ( buffer, client_id, &idx );
 return ( buffer->clients[idx].count );
}

RingBufferStatus ringBuf_write ( RingBuffer* buffer, const uint8_t* data, const size_t write_size )
{
    RingBufferStatus status = BUFF_OK;

    //Bloqueo el mutex aca
    mtx_lock ( &buffer->rw_mutex );

    // Si la cantidad de datos a copiar es m�s grande que el tama�o del buffer
    // genero un assert porque se estar�an sobreescribiendo datos antes de poder
    // leerlos
    assert ( buffer->buff_size >= write_size );

    // Pregunto si existen clientes
    if ( buffer->num_clients == 0 )
        status = BUFF_NO_CLIENTS;

    // Pregunto si no puedo sobreescribir
    if ( !buffer->can_override )
        // Pregunto si el buffer esta lleno o si no hay espacio disponible
        if ( is_ringBuf_full ( buffer ) )
            status = BUFF_FULL;
        if ( write_size > buffer->buff_size - buffer->count )
            status = BUFF_NOT_ENOUGH_SPACE;

    // Si no hubo errores
    if ( status == BUFF_OK )
    {
        // Obtengo el espacio que queda hacia la derecha del buffer
        uint32_t right_space = buffer->buff_size - ( buffer->head - buffer->buff );

        // Pregunto si lo que hay para escribir es menor que lo que queda hacia la derecha
        if ( write_size < right_space )
        {
            // Copio el dato
            memcpy ( buffer->head, data, write_size );

            // Actualizo el puntero
            buffer->head += write_size;
        }
        else
        {
            // Copio una parte y despu�s el resto al principio del buffer
            memcpy ( buffer->head, data, right_space );
            memcpy ( buffer->buff, data, write_size - right_space );

            // Actualizo el puntero
            buffer->head = &buffer->buff[ write_size - right_space ];
        }

        // Update info de clientes
        for ( uint32_t idx=0; idx < buffer->num_clients ; idx++ )
        {
            // Incremento el contador
            buffer->clients[idx].count += write_size;

            // Chequeo overrun
            if ( buffer->clients[idx].count > buffer->buff_size )
            {
                buffer->clients[idx].overrun = true;
                buffer->clients[idx].count = buffer->buff_size;
                buffer->clients[idx].ptr = buffer->head;
            }

            // Env�o un mensaje al cliente avisando que puede leer en caso que sea cierto
            if ( buffer->clients[idx].client_func != NULL && buffer->clients[idx].read_size <= buffer->clients[idx].count )
				buffer->clients[idx].client_func();
//                 osMessagePut ( buffer->clients[idx].msg_queue, buffer->clients[idx].msg_val, 0 );
        }
    }

    // Actualizo la cuenta
    ringBuf_updateCount ( buffer );

    // Libero el mutex aca
    mtx_unlock ( &buffer->rw_mutex );

    return status;
}

RingBufferStatus ringBuf_read_var ( RingBuffer* buffer, uint32_t client_id, size_t read_size, size_t shift_size, uint8_t* output )
{
    RingBufferStatus status;

    //Bloqueo el mutex aca
    mtx_lock ( &buffer->rw_mutex );

    // Busco el cliente
    uint32_t idx = 0;
    status = ringBuf_findClient ( buffer,client_id, &idx );

    // Si no hubo problemas leo el buffer
    if ( status == BUFF_OK )
        status = ringBuf_read ( buffer, &buffer->clients[idx].ptr, &buffer->clients[idx].count, read_size, shift_size, output );

    // Libero el mutex aca
    mtx_unlock ( &buffer->rw_mutex );

    return status;
}

RingBufferStatus ringBuf_read_fix ( RingBuffer* buffer, uint32_t client_id, uint8_t* output )
{
    RingBufferStatus status;

    //Bloqueo el mutex aca
    mtx_lock ( &buffer->rw_mutex );

    // Busco el cliente
    uint32_t idx = 0;
    status = ringBuf_findClient ( buffer,client_id, &idx );

    // Si no hubo problemas leo el buffer
    if ( status == BUFF_OK )
        status = ringBuf_read ( buffer, &buffer->clients[idx].ptr, &buffer->clients[idx].count, buffer->clients[idx].read_size, buffer->clients[idx].shift_size, output );

    // Libero el mutex aca
    mtx_unlock ( &buffer->rw_mutex );

    return status;
}

RingBufferStatus ringBuf_read_all ( RingBuffer* buffer, uint32_t client_id, uint8_t* output, size_t* read_size )
{
    RingBufferStatus status;

    //Bloqueo el mutex aca
    mtx_lock ( &buffer->rw_mutex );

    // Busco el cliente
    uint32_t idx = 0;
    status = ringBuf_findClient ( buffer,client_id, &idx );

    // Pregunto si lo que queda por leer es menor que lo que lee el cliente
    // (sino deber�a hacer un read_const primero)
    if ( buffer->clients[idx].read_size < buffer->clients[idx].count )
        status = BUFF_ERROR;

    // Si no hubo problemas leo el buffer
    if ( status == BUFF_OK )
    {

        // Salvo la cantidad que va a leer�
        *read_size = buffer->clients[idx].count;

        // Leo el buffer
        status = ringBuf_read ( buffer, &buffer->clients[idx].ptr, &buffer->clients[idx].count, buffer->clients[idx].count, buffer->clients[idx].count, output );
    }

    // Libero el mutex aca
    mtx_unlock ( &buffer->rw_mutex );

    return status;
}

//-----------------------------------------------------------
//						STATIC FUNCTIONS
//-----------------------------------------------------------
// The Static functions are not protected by a mutex

void ringBuf_updateCount ( RingBuffer *buffer )
{
    // Actualizo el contador
    uint32_t max_count = 0;
    for ( uint32_t idx=0; idx < buffer->num_clients ; idx++ )
        if ( buffer->clients[idx].count > max_count ) {
            max_count = buffer->clients[idx].count;
        }
    buffer->count = max_count;
}

RingBufferStatus ringBuf_findClient ( RingBuffer* buffer, const uint32_t client_id, uint32_t* idx )
{
    RingBufferStatus status;

    // Busco el cliente
    for ( *idx = 0; *idx < buffer->num_clients; (*idx)++ )
        if ( buffer->clients[*idx].client_id == client_id )
            break;

    // Si idx llego el n�mero de clientes significa que no lo encontr�
    if ( *idx == buffer->num_clients )
        status = BUFF_NOT_A_CLIENT;
    else
        status = BUFF_OK;

    return status;
}

RingBufferStatus ringBuf_read ( RingBuffer *buffer, uint8_t **ptr, uint32_t *count, size_t read_size, size_t shift_size, uint8_t *output )
{
    RingBufferStatus status = BUFF_OK;

    // Me fijo que lo que quiera leer no sea mayor al tama�o del buffer
    assert ( buffer->buff_size >= read_size );

    // Me fijo que se puedan leer la cantidad de datos que se piden
    if ( *count < read_size )
        status = BUFF_NOT_READY;
    else
    {
        // Obtengo el espacio que queda hacia la derecha del buffer
        uint32_t right_space = buffer->buff_size - ( *ptr - buffer->buff );

        // Pregunto si lo que se quiere leer es menor que lo que hay a la derecha
        if ( read_size < right_space )
            // Copio el dato
            memcpy ( output, *ptr, read_size );
        else
        {
            // Copio una parte y despu�s leo desde el principo del buffer
            memcpy ( output, *ptr, right_space );
            memcpy ( &output[right_space], buffer->buff, read_size - right_space );
        }

        // Actualizo el puntero (no con la cantidad de datos que ley�, sino con lo que quiera shiftear)
        if ( shift_size < right_space )
            *ptr += shift_size;
        else
            *ptr = &buffer->buff[ shift_size - right_space ];

        // Decremento el contador
        *count -= shift_size;
    }

    // Actualizo la cuenta
    ringBuf_updateCount ( buffer );

    return status;
}





