/**
 * *****************************************************************************
 * @file    ring_buffer_os.h
 * @author  Alejandro Alvarez
 * @version V1.0.0
 * @date    30-Abril-2016
 * @brief   Glue file for Operative System
 ******************************************************************************
 * @attention
 *
 ******************************************************************************
 */

#ifndef RING_BUFFER_OS_H
#define RING_BUFFER_OS_H

#include <stdlib.h>
#include <stdint.h>


/* Timeout value. */
#define osWaitForever     0xFFFFFFFF     ///< wait forever timeout value

typedef uint32_t mtx_t;

enum {
    mtx_plain,
};

typedef enum {
    Mutex_OK,
    Mutex_Error,
} Mutex_Status;


/** Create and Initialize a Mutex object.
 * @param[in]     mutex_def     mutex definition referenced with \ref osMutex.
 * @return mutex ID for reference by other functions or NULL in case of error.
 */
int mtx_init ( mtx_t* mutex, int type );

/** Wait until a Mutex becomes available.
 * @param[in]     mutex_id      mutex ID obtained by \ref osMutexCreate.
 * @param[in]     millisec      timeout value or 0 in case of no time-out.
 * @return status code that indicates the execution status of the function.
 */
int mtx_lock ( mtx_t* mutex );

/** Release a Mutex that was obtained by \ref osMutexWait.
 * @param[in]     mutex_id      mutex ID obtained by \ref osMutexCreate.
 * @return status code that indicates the execution status of the function.
 */
int mtx_unlock ( mtx_t* mutex );

/** Delete a Mutex that was created by \ref osMutexCreate.
 * @param[in]     mutex_id      mutex ID obtained by \ref osMutexCreate.
 * @return status code that indicates the execution status of the function.
 */
int mtx_destroy ( mtx_t* mutex );


#endif
