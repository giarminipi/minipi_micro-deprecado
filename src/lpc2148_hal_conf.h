/**
  ******************************************************************************
  * @file    lpc2148_hal_conf.h
  * @author  
  * @version 
  * @date    
  * @brief   
  ******************************************************************************
  * @attention
  *
  ******************************************************************************
	*/
	
/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef _LPC2148_HAL_CONF_H
#define _LPC2148_HAL_CONF_H
	
#define HAL_GPIO_MODULE_ENABLED
#define HAL_TIM_MODULE_ENABLED
#define HAL_SSP_MODULE_ENABLED
#define HAL_UART_MODULE_ENABLED
#define HAL_PWM_MODULE_ENABLED
#define HAL_SPI_MODULE_ENABLED

#endif // End _LPC2148_HAL_CONF_H */
