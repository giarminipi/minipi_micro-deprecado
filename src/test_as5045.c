/**
  ******************************************************************************
  * @file    	test_as5045.c
  * @author		Alejandro Gast�n Alvarez
  * @version	1.0
  * @date			11/04/2016
  * @brief		Programa de testeo del encoder AS5045
  ******************************************************************************
  * @attention
  *
  ******************************************************************************
	*/
#include <test_as5045.h>

uint32_t client_id;
AS5045_Data_Type* sensor;
RingBuffer as5045_buffer;

void leer_buffer(void)
{
	// Leo el buffer como qued� seteado el cliente
	ringBuf_read_fix ( &as5045_buffer, client_id, (uint8_t*) sensor );
	
	//Como leo de a dos puedo acceder utilizando sensor[0] y sensor[1]	
}

void test_as5045 (void)
{
	// Inicializo el driver del sensor (le paso el buffer que tiene que utilizar y un tama�o de 10 muestras)
	As5045_Init( &as5045_buffer, 10 );
	
	// Creo una variable donde se va a guardar el dato del sensor cuando realice la lectura del ringBuffer
	sensor = (AS5045_Data_Type*) malloc(sizeof(AS5045_Data_Type)*2);

	// Registro el cliente para que lea los dos sensores a la vez y que su puntero se desplaze tambi�n de a dos sensores a la vez ==> siempre lee informaci�n nueva
	ringBuf_registClient ( &as5045_buffer, sizeof(AS5045_Data_Type)*2, sizeof(AS5045_Data_Type)*2, (callback) leer_buffer, &client_id );
	
	while(1)
	{
		AS5045AskData ();
		for(uint32_t i=0; i<1000000; i++);
	}
}
