/**
  ******************************************************************************
  * @file    	test_ir_line.c
  * @author		Alejandro Gast�n Alvarez
  * @version	1.0
  * @date		08/10/2016
  * @brief		Programa de testeo de los sensores de l�nea
  ******************************************************************************
  * @attention
  *
  ******************************************************************************
  */
  
#include <ir_line.h>
#include <led.h>


void test_ir_line(void)
{
	initIrLinea();
	initLed();

	IR_Line_Data ir_data;
	while (1)
	{
		ir_data = get_ir_line_data();

		// Leo el sensor 1
		if (ir_data.ir.s1)
			ledOn(LED_1);
		else
			ledOff(LED_1);

		// Leo el sensor 2
		if (ir_data.ir.s2)
			ledOn(LED_2);
		else
			ledOff(LED_2);

		// Leo el sensor 3
		if (ir_data.ir.s3)
			ledToggle(LED_1);
		else
			ledOff(LED_1);

		// Leo el sensor 4
		if (ir_data.ir.s4)
			ledToggle(LED_2);
		else
			ledOff(LED_2);
	}
}
//*******************************************
