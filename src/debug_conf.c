/**
  ******************************************************************************
  * @file    debug_conf.h
  * @author  Alejandro Gast�n Alvaerez
  * @version 0.0
  * @date    05/04/2016
  * @brief   Debug definitions and configuration
  ******************************************************************************
  * @attention
  *
  ******************************************************************************
	*/

//#ifndef __assert_h
//void __aeabi_assert(const char *expr, const char *file, int line)
//{
//	abort();
//}
//#endif

//struct __FILE { int handle; /* Add whatever needed */ };
//FILE __stdout;
//FILE __stdin;


void printf_config (void)
{
	UART_Config UART1_Conf;
	P0ModeSelec (PIN_8 | PIN_9 , P0_MODE_UART1, 0); //| PIN_10 | PIN_11
	UART_ConfigStructInit(&UART1_Conf, 256000, UART_DATABIT_8, UART_PARITY_NONE, UART_STOPBIT_1, UART_FIFO_RX_TRGLEV0, UART_INTCFG_NONE);
	UART_Init(UART1, &UART1_Conf);

	uint8_t data, count = 0;
	char string[10];
	while (1)
	{
		UART_ReceiveByte(UART1, &data);

		if (data)
			string[count++] = data;

		if (count > 10)
			count = 0;

		if (strcmp( string, "sali" ) )
			break;
	}
	//*******************************************
}

