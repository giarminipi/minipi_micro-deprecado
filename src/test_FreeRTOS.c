	//*******************************************
	//				TEST FREERTOS
	//*******************************************
//	/* Create one of the two tasks. Note that a real application should check
//	the return value of the xTaskCreate() call to ensure the task was created
//	successfully. */
//	xTaskCreate ( testTask, "TestTask", 128, NULL, 1, NULL );
//	xTaskCreate ( testTask2, "TestTask2", 128, NULL, 1, NULL );
//
//
//	/* Start the scheduler so the tasks start executing. */
//	vTaskStartScheduler();
//
//	/* If all is well then main() will never reach here as the scheduler will
//	now be running the tasks. If main() does reach here then it is likely that
//	there was insufficient heap memory available for the idle task to be created.
//	CHAPTER 5 provides more information on memory management. */
//	for( ;; );

	//*******************************************

void testTask ( void *pvParameters );
void testTask2 ( void *pvParameters );


void testTask ( void *pvParameters )
{
	int i=0;

	for( ;; )
	{
		i++;
	}

	/* Tasks must not attempt to return from their implementing
	function or otherwise exit.  In newer FreeRTOS port
	attempting to do so will result in an configASSERT() being
	called if it is defined.  If it is necessary for a task to
	exit then have the task call vTaskDelete( NULL ) to ensure
	its exit is clean. */
//	vTaskDelete( NULL );
}

void testTask2 ( void *pvParameters )
{
	int i=0;

	for( ;; )
	{
		i++;
	}

	/* Tasks must not attempt to return from their implementing
	function or otherwise exit.  In newer FreeRTOS port
	attempting to do so will result in an configASSERT() being
	called if it is defined.  If it is necessary for a task to
	exit then have the task call vTaskDelete( NULL ) to ensure
	its exit is clean. */
//	vTaskDelete( NULL );
}
