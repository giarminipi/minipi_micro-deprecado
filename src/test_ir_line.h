/**
  ******************************************************************************
  * @file    	test_ir_line.h
  * @author		Alejandro Gast�n Alvarez
  * @version	1.0
  * @date			08/10/2016
  * @brief		Programa de testeo de los sensores de l�nea
  ******************************************************************************
  * @attention
  *
  ******************************************************************************
  */
  
/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef _TEST_IR_LINE_H
#define _TEST_IR_LINE_H

void test_ir_line (void);

#endif	//_TEST_IR_LINE_H
