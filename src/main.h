/**
  ******************************************************************************
  * @file    main.h
  * @author  
  * @version 
  * @date    
  * @brief   
  ******************************************************************************
  * @attention
  *
  ******************************************************************************
	*/
	
#ifndef _MAIN_H_
#define _MAIN_H_

// funcion para prueba de timer
void setLeds (short led2,short led1,short led0);
#define     LED0        PIN_3 /* binary: 00000000_00000000_00000000_00001000 */
#define     LED1        PIN_4 /* binary: 00000000_00000000_00000000_00010000 */
#define     LED2        PIN_5 /* binary: 00000000_00000000_00000000_00100000 */

void testTask ( void *pvParameters );
void testTask2 ( void *pvParameters );

/**
  * @brief  Selector de Comandos
  * @param  COM, es el número de comando
  * @param  Parametro, es que modificador que acompaña al comando
  * @retval 
	*/
void SEL_COM (short COM,short Parametro);
#endif 	// end _MAIN_H_
