/**
  ******************************************************************************
  * @file    	test_as5045.h
  * @author		Alejandro Gast�n Alvarez
  * @version	1.0
  * @date			11/04/2016
  * @brief		Programa de testeo del encoder AS5045
  ******************************************************************************
  * @attention
  *
  ******************************************************************************
  */
  
/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef _TEST_AS5045_H
#define _TEST_AS5045_H

#include <As5045.h>

void test_as5045 (void);
void leer_buffer(void);

#endif	//_TEST_AS5045_H
