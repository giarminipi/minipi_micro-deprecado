// **************  TABLA  *************************************************************
unsigned char INPUT1=0x01,INPUT2=0x01, INPUTTEMP=0x00;
unsigned char STATE= 0x00;
unsigned char M_DER=50,M_IZQ=50;
unsigned char Planilla [18][6] =
{
				{0x00,0x00,0x00,70,70,0x00},  // sin obstaculo: adelante
        {0x00,0x00,0x10,30,70,0x00},	// Ost�culo, gira +
        {0x00,0x00,0x20,70,70,0x00},	// sin obstaculo: adelante
        {0x00,0x00,0x30,70,30,0x00},	// Ost�culo, gira -
        {0x00,0x00,0x40,70,70,0x00},  // sin obstaculo: adelante
				{0x00,0x01,0x50,30,70,0x00},  // Ost�culo, gira +
				{0x00,0x00,0x60,70,70,0x00},  // sin obstaculo: adelante
				{0x00,0x00,0x70,70,30,0x00},  // Ost�culo, gira -
				{0x00,0x00,0x80,70,70,0x00},  // sin obstaculo: adelante
        {0x00,0x00,0x90,30,70,0x00},	// Ost�culo, gira +
        {0x00,0x00,0xA0,70,70,0x00},	// sin obstaculo: adelante
        {0x00,0x00,0xB0,30,30,0x00},	// Encrucijada, Retrocede
        {0x00,0x00,0xC0,70,70,0x00},  // sin obstaculo: adelante
				{0x00,0x01,0xD0,30,70,0x00},  // Ost�culo, gira +
				{0x00,0x00,0xE0,70,70,0x00},  // sin obstaculo: adelante
				{0x00,0x00,0xF0,50,50,0x00},  // Bloqueado. Se detiene
        {0xFF,0x00,0x01,0x20,0x20,0x00}


//				{0x00,0x00,0x00,70,70,0x00},  // PROTOTIPO DE MAQUINA DE ESTADOS
//        {0x00,0x00,0x01,30,30,0x01},	// Primer campo: 	Estado
//        {0x00,0x00,0x02,30,30,0x01},	// Segundo campo:	INPUT	(variables de entrada del sistema)
//        {0x00,0x00,0x03,30,30,0x01},	// Tercer campo:	INPUT2(variables de entrada del sistema)
//        {0x00,0x00,0x0F,50,50,0x02},  // Cuarto campo:	Motor izquierdo (salida)
//				{0x00,0x01,0x00,80,20,0x00},  // Cuarto campo:	Motor izquierdo (salida)
//				{0x01,0x00,0x00,30,30,0x01},  // Quinto campo:	Motor derecho (salida)
//				{0x01,0x00,0x04,70,70,0x00},  // Sexto campo:		Estado proximo
//        {0x01,0x00,0x08,70,70,0x00},  //
//        {0x01,0x00,0x0C,70,70,0x00},	//
//        {0x01,0x00,0x0F,50,50,0x02},
//				{0x02,0x00,0x00,50,50,0x02},
//        {0xFF,0x00,0x01,0x20,0x20,0x00}

};




//	init_pll();
//	init_T0_T1();
//	bool flag = true;


//	// Seteo el vector de interrupci�n del timer 0
//	VIC_EnableIRQ(TIMER0_IRQn, 2, (uint32_t*)test_irq, false);


//	//*******************************************
//	//				Main Maquina de Estados
//	//*******************************************

//	float pwm_tick = 1/30.0;	// Tick o Bin del m�dulo del PWM (cuanto tiempo dura una cuenta) [mseg]
//	float pwm_freq = 20;			// Frecuencia del PWM [KHz]
//	uint32_t duty = 50;				// Duty cycle del PWM
//
//	// Seteo los pines P0.0 y P0.7 como PWM
//	P0ModeSelec (PIN_0, P0_MODE_PWM, 0);
//	P0ModeSelec (PIN_7, P0_MODE_PWM, 0);
//
//	// Inicializo los PWM
//	initPWM(pwm_tick, pwm_freq );
//
//	// Configuro los PWM1 y PWM2
//	configPWMxChannel(PWM1, PWM_SINGLE_EDGE, PWN_NONE, duty, 0, 0); // duty en porcentaje y offset en microsegundos;
//	configPWMxChannel(PWM2, PWM_SINGLE_EDGE, PWN_NONE, duty, 0, 0); // duty en porcentaje y offset en microsegundos;
//
//	startPWM();
//	enablePWMxChannel(PWM1);
//	enablePWMxChannel(PWM2);
//
//		initLed();
//
//	init_T0();						//se inicializa Timer 0
//	init_T1();						//se inicializa Timer 1 (por ahora, despu�s va a ser una tarea)
//	init_Sensores_US();  	//se inicializa sensores de ultrasonido
//
//	initIrLinea();	//se inicializa sensores de l�nea
//	IR_Line_Data ir_data;
//
//	// Inicializacion Maquina de Estados  **********************************************
//	int i;
//	int tics_US_MAX= 5000, tics_US_min= 500;			// Se define una distancia M�xima, m�nima de medici�n
//
//	while (1)
//	{
//		//*********************************** Maquina de Estados **********************************************
//		INPUTTEMP=0x00;
//
//		int j=0;

//		while (j!=10000)				// Delay para ver c�mo se mueve
//			j++;
//
//
//		i=0;
//		sensar_US ();
//		// Ultrasonido1 dentro del rango
//		if( tics_US_1 > tics_US_min  && tics_US_1  < tics_US_MAX )
//		{
//			INPUTTEMP=INPUTTEMP | 0x10;
//			ledOn(LED_2);
//		}
//		else
//		{
//			INPUTTEMP =  INPUTTEMP & 0xEF ;
//			ledOff(LED_2);
//		}
//
//
//		// Ultrasonido2 dentro del rango
//		if( tics_US_2 > tics_US_min  && tics_US_2  < tics_US_MAX )
//		{
//			INPUTTEMP=INPUTTEMP | 0x20;
////			ledToggle(LED_2);
//		}
//		else
//		{
//			INPUTTEMP =  INPUTTEMP & 0xDF ;
//		}
//
//
//		// Ultrasonido3 dentro del rango
//		if( tics_US_3 > tics_US_min  && tics_US_3  < tics_US_MAX )
//		{
//			INPUTTEMP=INPUTTEMP | 0x40;
////			ledToggle(LED_2);
//		}
//		else
//		{
//			INPUTTEMP =  INPUTTEMP & 0xBF ;
//		}
//
//
//		// Ultrasonido4 dentro del rango
//		if( tics_US_4 > tics_US_min  && tics_US_4  < tics_US_MAX )
//		{
//			INPUTTEMP=INPUTTEMP | 0x80;
////			ledToggle(LED_2);
//		}
//		else
//		{
//			INPUTTEMP =  INPUTTEMP & 0x7F ;
//		}
//
//
//		ir_data = get_ir_line_data();	// lee sensores de l�nea y los almacena en INPUT1 e INPUT2
//
//
//		INPUT1 = 0;		// Byte adicional de sensores
//
//		//INPUT2 = ir_data.ir.s1 + 2*ir_data.ir.s2 + 4*ir_data.ir.s3 + 8*ir_data.ir.s4; // convierte los bits de cada sensor en un byte de sensores
//		INPUT2 = 0x0F & ir_data.line_sensor; // convierte los bits de cada sensor en un byte de sensores
//    INPUT2 = ~INPUT2;   // invierte los bits
//		INPUT2 = INPUT2 & 0x0F;  // enmascara el byte
//
//		INPUT2= INPUTTEMP;			// ac� copiamos INPUTTEMP en INPUT2 perdiendo la informaci�n de los sensores de l�nea
//
//
//		// Rutina MdE
//		// recorre la tabla, hasta que encuentra un Estado FF
//		while(Planilla[i][0]!=0xFF )
//		{
//			if(STATE == Planilla[i][0] )
//			{
//				// coincide la entrada con la combinaci�n de la Planilla?
//        if( ( (Planilla[i][1]) == INPUT1) && ( (Planilla[i][2] == INPUT2) ))
//				{
//
//					M_DER=Planilla[i][3];
//          M_IZQ=Planilla[i][4];
//          STATE= Planilla[i][5];
//					//PWM_PORCENTUAL(M_DER,M_IZQ);

//					updatePWMxChannel(PWM1, M_DER, 0);				// setea motor1
//					updatePWMxChannel(PWM2, M_IZQ, 0);				// setea motor2
//					break; // sale del while porque encontr� el ESTADO y al condici�n coincidente en la TABLA
//
//				}
//			}
//      i=i+1;
//     }
//
//         // ********************** Fin rutina Maquina de Estados ********************************
//
//	}