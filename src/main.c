/**
  ******************************************************************************
  * @file    	main.c
  * @author  ale
  * @version 
  * @date    
  * @brief   
  ******************************************************************************
  * @attention
  *
  ******************************************************************************
	*/
	
#include <main.h>
#include <stdlib.h>
#include <stdio.h>

#include <lpc2148_hal.h>
////#include <stdbool.h>
//#include <led.h>
//#include <ir_line.h>
////#include <string.h>
//#include <ultrasound.h>
////#include <FreeRTOS.h>
////#include <task.h>
//#include <MdE.h>

////#include <test_as5045.h>
//#include <As5045.h>
//#include <string.h>

#include <test_ir_line.h>

#ifndef __assert_h
void __aeabi_assert(const char *expr, const char *file, int line)
{
	abort();
}
#endif

struct __FILE { int handle; /* Add whatever needed */ };
FILE __stdout;
FILE __stdin;

	
int main (void)
{
	// Configure pll
	configurePLL (12, 60);
	setAPBDivider (DIV_2);
				
	// Check pll configuration
	uint32_t osc = oscFreq();
	uint32_t clk = clkFreq();
	uint32_t per = perClk();
	
	//Programa de testeo de lineas
	test_ir_line();
	
	while(1)
	{
	}

	
		
//	float pwm_tick = 1/30.0;	// Tick o Bin del módulo del PWM (cuanto tiempo dura una cuenta) [mseg]
//	float pwm_freq = 20;			// Frecuencia del PWM [KHz]
//	uint32_t duty = 50;				// Duty cycle del PWM
//// ***** Se Inicializan y Configuran los PWM1 y PWM2
//P0ModeSelec (PIN_0, P0_MODE_PWM, 0);//	Se Setea los pines P0.0 y P0.7 como PWM
//P0ModeSelec (PIN_7, P0_MODE_PWM, 0);

//initPWM(pwm_tick, pwm_freq);

//configPWMxChannel(PWM1, PWM_SINGLE_EDGE, PWN_NONE, duty, 0, 0); // duty en porcentaje y offset en microsegundos;
//configPWMxChannel(PWM2, PWM_SINGLE_EDGE, PWN_NONE, duty, 0, 0); // duty en porcentaje y offset en microsegundos;
//	
//startPWM();
//enablePWMxChannel(PWM1);
//enablePWMxChannel(PWM2);

//// ***** se inicializan sensores de línea
//initIrLinea();	
//IR_Line_Data ir_data;
//initLed();

//// ***** se inicializan sensores de US	
//	
//	init_T1();
//  init_Timer (2, TIMER_0, MODE_CAP_BOTH, PIN_6, 2);
//  init_Timer (2, TIMER_0, MODE_CAP_BOTH, PIN_22, 2);
//	init_Sensores_US();
//	

//// ***** Se inicializa Variables Maquina de Estados  **********************************************
//unsigned char INPUT1=0x01,INPUT2=0x01, INPUTTEMP=0x00;
//unsigned char STATE= 0x00;
//unsigned char M_DER=50,M_IZQ=50;
//int i;
//int tics_US_MAX= 5000, tics_US_min= 500;			// Se define una distancia Máxima, mínima de medición
//int Rango;


//while (1)
//{
//		//*********************************** Maquina de Estados ********************************************** 
//		INPUTTEMP=0x00;
//		
//		// ******************
////		int j=0;
////		while (j!=10000)				// Delay para ver cómo se mueve
////		j++;
//		// ******************			

//			
//		// ***** Se comparan los sensores de US contra una ventana de tics_US_MAX y tics_US_min
//		i=0;                       											
//		sensar_US ();						// habilita lectura US
//		Rango = read_US_1();		// Lee Ultrasonido1 dentro del rango		
//		if( Rango > tics_US_min  && Rango  < tics_US_MAX )
//		{				
//			INPUTTEMP=INPUTTEMP | 0x10;
//			ledOn(LED_2);
//		}
//		else
//		{
//			INPUTTEMP =  INPUTTEMP & 0xEF ;
//			ledOff(LED_2);
//		}
//			
//		Rango = read_US_2();		// Lee Ultrasonido2 dentro del rango
//		if( Rango > tics_US_min  && Rango  < tics_US_MAX )
//		{
//			INPUTTEMP=INPUTTEMP | 0x20;
//		}
//		else
//		{
//			INPUTTEMP =  INPUTTEMP & 0xDF ;
//		}

//		
//		Rango = read_US_3();		// Lee Ultrasonido3 dentro del rango	
//		if( Rango > tics_US_min  && Rango  < tics_US_MAX )
//		{
//			INPUTTEMP=INPUTTEMP | 0x40;
////			ledToggle(LED_2);
//		}
//		else
//		{
//			INPUTTEMP =  INPUTTEMP & 0xBF ;
//		}
//		
//		Rango = read_US_4();		// Lee Ultrasonido4 dentro del rango	
//		if( Rango > tics_US_min  && Rango  < tics_US_MAX )
//		{
//			INPUTTEMP=INPUTTEMP | 0x80;
////			ledToggle(LED_2);
//		}
//		else
//		{
//			INPUTTEMP =  INPUTTEMP & 0x7F ;
//		}
//	

//		INPUT1 = 0;		// Byte adicional de sensores, por ahora queda en 0
//		
//		// ***** Se leen sensores de línea
//		ir_data = get_ir_line_data();	// lee sensores de línea y para almacenalos en INPUT2
//		//INPUT2 = ir_data.ir.s1 + 2*ir_data.ir.s2 + 4*ir_data.ir.s3 + 8*ir_data.ir.s4; // convierte los bits de cada sensor en un byte de sensores
//		INPUT2 = 0x0F & ir_data.line_sensor; // convierte los bits de cada sensor en un byte de sensores
//    INPUT2 = ~INPUT2;   // invierte los bits
//		INPUT2 = INPUT2 & 0x0F;  // enmascara el byte
//		
//		INPUT2= INPUTTEMP;			// acá copiamos INPUTTEMP en INPUT2 perdiendo la información de los sensores de línea para hacer pruebas
//			
//			
//		
//		// *************** Rutina MdE. recorre la tabla, hasta que encuentra un Estado FF *****************
//		while(Planilla[i][0]!=0xFF )
//		{
//			if(STATE == Planilla[i][0] )
//			{ 
//				// coincide la entrada con la combinación de la Planilla?                 
//        if( ( (Planilla[i][1]) == INPUT1) && ( (Planilla[i][2] == INPUT2) ))
//				{
//				
//					M_DER=Planilla[i][3];    
//          M_IZQ=Planilla[i][4];
//          STATE= Planilla[i][5];
//					//PWM_PORCENTUAL(M_DER,M_IZQ);

//					SEL_COM (M_DER,M_IZQ);				// setea COM 
////					updatePWMxChannel(PWM2, M_IZQ, 0);				// setea motor2
//					break; // sale del while porque encontró el ESTADO y la condición coincidente en la TABLA			
//									
//				}
//			}
//      i=i+1;
//     }
//                
//         // ********************** Fin rutina Maquina de Estados ********************************   
//	
//	
//	}
 }
	
/** ###################################################################
**     Nombre   : SEL_COM
**     Resumen : selecciona COMANDOS a partir de un número de Comando y un parámetro modificador.
** ###################################################################*/
void SEL_COM (short COM,short Parametro){
switch(COM)
{
case 1:
		// Setea PWM1 y PWM2 para ir para ADELANTE
		updatePWMxChannel(PWM1, 50+Parametro*0.50, 0);				// setea motor1 
		updatePWMxChannel(PWM2, 50+Parametro*0.50, 0);				// setea motor2
    break;
case 2:
		// Setea PWM1 y PWM2 para girar a la Izq
		updatePWMxChannel(PWM1, 50-Parametro*0.50, 0);				// setea motor1 
		updatePWMxChannel(PWM2, 50+Parametro*0.50, 0);				// setea motor2
		break;
case 3:
		// Setea PWM1 y PWM2 para girar a la Der
		updatePWMxChannel(PWM1, 50+Parametro*0.50, 0);				// setea motor1 
		updatePWMxChannel(PWM2, 50-Parametro*0.50, 0);				// setea motor2
		break;
case 4:
		// Setea PWM1 y PWM2 para RETROCEDER
		updatePWMxChannel(PWM1, 50-Parametro*0.50, 0);				// setea motor1 
		updatePWMxChannel(PWM2, 50-Parametro*0.50, 0);				// setea motor2
		break;

default:
	// error: comando no soportado 
    break;
}
}
