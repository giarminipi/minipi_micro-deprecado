/**
  ******************************************************************************
  * @file    	test_ssp.c
  * @author		Alejandro Gast�n Alvarez
  * @version	1.0
  * @date			11/04/2016
  * @brief		Programa de testeo del SSP
  ******************************************************************************
  * @attention
  *
  ******************************************************************************
	*/


	// Seteo los pines P0.0 y P0.7 como PWM
//	P0ModeSelec (PIN_17 | PIN_18 | PIN_19 | PIN_20, P0_MODE_SSP, 0);

//	// Pulling mode
//	initSSP(SSP_Master, SSP_SSI, CLK_POL_HIGH, SSP_PH_FRST, SSP_Tx09, 20000, SSP_IRQ_DISABLE, 0);
//	SSP_LoopBack_Enable (true);
//	SSP_Enable (true);
//	int data = 0x5A;
//	while(writeSSP (data) != SSP_OK);
//	data = 0;
//	while(readSSP(&data) != SSP_OK);

//	//Interrupt mode
//	deinitSSP();
//	initSSP(SSP_Master, SSP_SSI, CLK_POL_HIGH, SSP_PH_FRST, SSP_Tx09, 20000, SSP_IRQ_Rx_Half_Full, 6);
//	SSP_LoopBack_Enable (true);
//	SSP_Enable (true);
//	data = 0;
//	while(1)
//	{
//		if (writeSSP (data) == SSP_OK)
//			data++;
//	}

	
	
	//void SSP_IRQ (void)
//{
//	bool error = false;
//	if ( IS_SSP_IRQ_Rx_Full() )
//		error = true;

//	int data = 0;
//	while(readSSP(&data) == SSP_OK);
//}
